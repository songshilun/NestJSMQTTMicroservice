import { Injectable } from '@nestjs/common';
import Minio from 'minio';
import { Stream } from 'stream';

@Injectable()
export class MinioService {
  private client: Minio.Client;

  constructor() {
    // const endpoint = this.configService.get<string>('MINIO_ENDPOINT');
    // const accessKey = this.configService.get<string>('MINIO_ACCESS_KEY');
    // const secretKey = this.configService.get<string>('MINIO_SECRET_KEY');
    // const useSSL = this.configService.get<boolean>('MINIO_USE_SSL', false);
    // const port = this.configService.get<number>('MINIO_PORT', 9000);
    // this.client = new Minio.Client({
    //   endPoint: endpoint,
    //   port: port,
    //   useSSL: useSSL,
    //   accessKey: accessKey,
    //   secretKey: secretKey,
    // });
  }

  async uploadFile(bucketName: string, objectName: string, filePath: string) {
    try {
      await this.client.fPutObject(bucketName, objectName, filePath, {
        'Content-Type': 'application/octet-stream',
      });
      console.log(`File uploaded successfully: ${bucketName}/${objectName}`);
      return `File uploaded successfully: ${bucketName}/${objectName}`;
    } catch (err) {
      console.error('Error uploading file:', err);
      throw err;
    }
  }

  async downloadFile(bucketName: string, objectName: string, filePath: string) {
    try {
      await this.client
        .fGetObject(bucketName, objectName, filePath, null)
        .catch();
    } catch (err) {
      console.error('Error downloading file:', err);
      throw err;
    }
  }

  async deleteFile(bucketName: string, objectName: string) {
    try {
      await this.client.removeObject(bucketName, objectName);
      console.log(`File deleted successfully: ${bucketName}/${objectName}`);
      return `File deleted successfully: ${bucketName}/${objectName}`;
    } catch (err) {
      console.error('Error deleting file:', err);
      throw err;
    }
  }

  // 其他 MinIO 操作的方法...
}
