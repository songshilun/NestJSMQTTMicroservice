import { SetMetadata } from '@nestjs/common';

export const SkipTokenAuth = () => SetMetadata('skipTokenAuth', true);