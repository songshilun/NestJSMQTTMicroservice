import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { TokenExpiredError } from 'jsonwebtoken';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class TokenAuthGuard implements CanActivate {
  private readonly config = new ConfigService();
  private readonly logger = new Logger(TokenAuthGuard.name);
  constructor(
    private readonly reflector: Reflector,
    private readonly jwtService: JwtService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    // 检查路由处理程序是否带有 @SkipTokenAuth() 装饰器
    const skipTokenAuth = this.reflector.get<boolean>(
      'skipTokenAuth',
      context.getHandler(),
    );

    if (
      request.headers['referer'] != null &&
      request.headers['referer'].includes('swagger')
    ) {
      const payload = {
        username: 'bfxsy',
        sub: 1,
        role: 1,
      };
      request.user = payload; // 将解码后的用户信息存储在请求中
      return true; // 跳过令牌验证
    }

    if (skipTokenAuth) {
      return true; // 跳过令牌验证
    }

    // 检查请求头中是否存在授权头部信息
    const authorization = request.headers.authorization;
    if (!authorization) {
      this.logger.log(
        'TokenAuthGuard' +
          '请求IP地址为:' +
          request.connection.remoteAddress +
          ':' +
          request.connection.remotePort +
          '的用户请求了一个需要授权的接口,但是没有提供授权信息',
      );
      throw new UnauthorizedException('Missing authorization header');
    }

    // 提取令牌
    const token = authorization.replace('Bearer ', '');
    try {
      // 验证令牌
      const payload = await this.jwtService.verifyAsync(token, {
        secret: this.config.getJwtConfig().secret,
      });
      request.user = payload; // 将解码后的用户信息存储在请求中
      return true;
    } catch (err) {
      if (err instanceof TokenExpiredError) {
        this.logger.log(
          'TokenAuthGuard' +
            '请求IP地址为:' +
            request.connection.remoteAddress +
            ':' +
            request.connection.remotePort +
            '的用户请求了一个需要授权的接口,但是没有提供授权信息',
        );
        throw new UnauthorizedException('Token expired');
      }
      this.logger.log(
        'TokenAuthGuard' +
          '请求IP地址为:' +
          request.connection.remoteAddress +
          ':' +
          request.connection.remotePort +
          '的用户请求了一个需要授权的接口,但是没有提供授权信息',
      );
      throw new UnauthorizedException('Invalid token');
    }
  }
}
