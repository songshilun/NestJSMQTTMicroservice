import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
const bcrypt = require('bcryptjs');

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
  ) {}

  async auth(username: string, sub: number, role: string) {
    const payload = {
      username: username,
      sub: sub,
      role: role,
    };
    // const options = {
    //   expiresIn: '60s', // 设置过期时间，可以使用字符串或数字
    //, options
    // };
    var token = this.jwtService.sign(payload);
    console.log(token);
    return token;
  }
}
