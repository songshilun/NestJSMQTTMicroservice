import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { TokenAuthGuard } from './auth/guard/token.authguard';
import { JwtModule } from '@nestjs/jwt';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GlobalExceptionFilter } from './interceptor/errors.interceptor';
import { TransformInterceptor } from './interceptor/transform.interceptor';
import { ConfigService } from './config/config.service';
import { ConfigModule } from './config/config.module';
import { SystemModule } from './system/system.module';
import { RoleModule } from './role/role.module';
import { UserModule } from './user/user.module';
@Module({
  imports: [
    ConfigModule,
    JwtModule,
    AuthModule,
    SystemModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.getDatabaseConfig().host,
        port: configService.getDatabaseConfig().port,
        username: configService.getDatabaseConfig().username,
        password: configService.getDatabaseConfig().password,
        database: configService.getDatabaseConfig().databaseName,
        entities: [__dirname + '/**/*.entity{.ts,.js}'], // 实体类文件所在位置，自动加载实体
        synchronize: false, // 自动创建表
        logging: configService.getDatabaseConfig().logging, // 打印日志
        // 其他连接选项...
      }),
      inject: [ConfigService],
    }),
    RoleModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: GlobalExceptionFilter,
    },
    {
      provide: APP_GUARD,
      useClass: TokenAuthGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
    //Logger,
  ],
})
export class AppModule {}
