// import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

// @Entity({
//   name: 'user',
// })
// export class User {
//   @PrimaryGeneratedColumn({
//     comment: '用户ID',
//   })
//   id: number;

//   @Column({
//     comment: '姓名',
//   })
//   name: string;

//   @Column({
//     comment: '账号',
//   })
//   account: string;

//   @Column({
//     comment: '密码',
//   })
//   password: string;

//   @Column({
//     comment: '角色',
//   })
//   role: string;

//   @Column({
//     comment: '用户状态',
//   })
//   user_state: number;

//   @Column({
//     comment: '登陆状态',
//   })
//   login_state: number;

//   @Column({
//     comment: '手机号',
//   })
//   phone_number: string;

//   @Column({
//     comment: '部门',
//   })
//   department: string;

//   @Column({
//     comment: '默认密码',
//   })
//   default_password: string;

//   @Column({
//     comment: '账号权限',
//   })
//   permissions: string;

//   @Column({
//     comment: '操作人用户id',
//   })
//   add_user_id: number;

//   @Column({
//     comment: '操作人',
//   })
//   add_user: string;

//   @Column({
//     comment: '创建时间',
//   })
//   create_at: Date;

//   @Column({
//     comment: '更新时间',
//   })
//   update_at: Date;
// }
