import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto, ResetPassword } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { SkipTokenAuth } from 'src/auth/guard/skip-token-auth.decorator';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { LoginOutDto, UserLoginDto } from './dto/create-user.dto';
import { RequestWithUser } from 'src/auth/interface/requestWithUser';
import { IpAddress } from 'src/interceptor/ipaddress';

@Controller('User')
@ApiTags('鉴权与用户')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiOperation({ summary: '用户管理-新增', description: '新建用户信息' })
  create(
    @Body() createUserDto: CreateUserDto,
    @Req() requestwithUser: RequestWithUser,
  ) {
    return this.userService.create(createUserDto, requestwithUser);
  }

  @Get()
  @ApiOperation({ summary: '用户管理-获取列表', description: '获取列表' })
  findAll() {
    return this.userService.findAll();
  }

  @Get(':id')
  @ApiOperation({
    summary: '用户管理-获取单条数据',
    description: '获取单条数据',
  })
  findOne(@Param('id') id: string) {
    return this.userService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({
    summary: '用户管理-更新对应数据',
    description: '更新对应数据',
  })
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @Req() requestwithUser: RequestWithUser,
  ) {
    return this.userService.update(+id, updateUserDto, requestwithUser);
  }

  @Delete(':id')
  @ApiOperation({
    summary: '用户管理-删除对应数据',
    description: '删除对应数据',
  })
  remove(@Param('id') id: string) {
    return this.userService.remove(+id);
  }

  @Post('Login')
  @SkipTokenAuth()
  @ApiOperation({ summary: '登录', description: '登录请求接口' })
  async login(@IpAddress() ip: string, @Body() UserLoginDto: UserLoginDto) {
    return this.userService.Login(UserLoginDto, ip);
  }

  @Post('Logout')
  @ApiOperation({ summary: '登录下线', description: '登录下线请求接口' })
  async Loginout(@Body() LoginOutDto: LoginOutDto) {
    return this.userService.LogOut(LoginOutDto);
  }

  @Post('ResetPassword')
  @ApiOperation({ summary: '重置密码', description: '重置密码请求接口' })
  async ResetPassword(@Body() ResetPassword: ResetPassword) {
    return this.userService.ResetPassword(ResetPassword);
  }
}
