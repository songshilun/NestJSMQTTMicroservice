import {
  CreateUserDto,
  LoginOutDto,
  ResetPassword,
  UserLoginDto,
} from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { RequestWithUser } from 'src/auth/interface/requestWithUser';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class UserService {
  private client: ClientProxy;
  private readonly logger = new Logger(UserService.name);
  constructor(private readonly configService: ConfigService) {}

  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
    this.logger.log('用户管理服务消息队列通讯初始化完成...');
  }

  async create(createUserDto: CreateUserDto, requestWithUser: RequestWithUser) {
    const user = requestWithUser.user;
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户管理新建请求],请求的数据是:数据:${createUserDto},正在推送至事件中心`,
    );
    return await this.client.send<CreateUserDto>('MessageHub-User-Create', {
      createUserDto,
      user,
    });
  }

  async findAll() {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户管理获取列表请求],正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-User-FindAll', '');
  }

  async findOne(id: number) {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户管理获取单条数据请求],请求的数据是:[主键ID]:${id},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-User-FindOne', id);
  }

  async update(
    id: number,
    updateUserDto: UpdateUserDto,
    requestWithUser: RequestWithUser,
  ) {
    const user = requestWithUser.user;
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户管理更新请求],请求的数据是:[主键ID]:${id},数据:${JSON.stringify(
        updateUserDto,
      )},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-User-Update', {
      id,
      updateUserDto,
      user,
    });
  }

  async remove(id: number) {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户管理删除请求],请求的数据是:[主键ID]:${id},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-User-Remove', id);
  }

  async Login(UserLoginDto: UserLoginDto, ip: string) {
    //send用于发送请求并等待响应。
    //emit用于发布事件并不等待响应。
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户登录请求],登录的信息是:[用户名]${UserLoginDto.username} [密码]${UserLoginDto.password},正在推送至事件中心`,
    );
    return await this.client.send<UserLoginDto>('MessageHub-User-login', {
      ip,
      UserLoginDto,
    });
  }

  async LogOut(LoginOutDto: LoginOutDto) {
    //send用于发送请求并等待响应。
    //emit用于发布事件并不等待响应。
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户登录下线请求],下线的用户是${LoginOutDto.user_id},正在推送至事件中心`,
    );
    return await this.client.send<LoginOutDto>(
      'MessageHub-User-logOut',
      LoginOutDto,
    );
  }

  async ResetPassword(ResetPassword: ResetPassword) {
    //send用于发送请求并等待响应。
    //emit用于发布事件并不等待响应。
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[用户重置密码下线请求],要重置密码的用户是${
        ResetPassword.user_id
      },[数据]:${JSON.stringify(ResetPassword)},正在推送至事件中心`,
    );
    return await this.client.send(
      'MessageHub-User-ResetPassword',
      ResetPassword,
    );
  }
}
