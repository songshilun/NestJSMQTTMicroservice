import { ApiExtraModels, ApiProperty } from '@nestjs/swagger';

@ApiExtraModels()
export class CreateUserDto {
  @ApiProperty({
    description: '姓名',
  })
  name: string;

  @ApiProperty({
    description: '账号',
  })
  account: string;

  @ApiProperty({
    description: '密码',
  })
  password: string;

  @ApiProperty({
    description: '角色',
  })
  role: number;

  @ApiProperty({
    description: '关联角色表id',
  })
  ass_user_role_id: number;

  @ApiProperty({
    description: '用户状态',
  })
  user_state: number;

  @ApiProperty({
    description: '登陆状态',
  })
  login_state: number;

  @ApiProperty({
    description: '手机号',
  })
  phone_number: string;

  @ApiProperty({
    description: '部门',
  })
  department: string;

  @ApiProperty({
    description: '默认密码',
  })
  default_password: string;

  @ApiProperty({
    description: '账号权限',
  })
  permissions: string;

  @ApiProperty({
    description: '添加人用户ID',
  })
  add_user_id: number;

  @ApiProperty({
    description: '添加人名称',
  })
  add_user: string;

  @ApiProperty({
    description: '新建时间',
  })
  create_at: Date;

  @ApiProperty({
    description: '更新时间',
  })
  update_at: Date;
}

export class UserLoginDto {
  @ApiProperty({
    description: '用户名',
  })
  username: string;

  @ApiProperty({
    description: '密码',
  })
  password: string;
}

export class LoginOutDto {
  @ApiProperty({
    description: '用户ID',
  })
  user_id: string;
}

export class ResetPassword {
  /**
   * 用户id
   */
  @ApiProperty({
    description: '用户id',
  })
  user_id: number;

  /**
   * 旧密码
   */
  @ApiProperty({
    description: '旧密码',
  })
  old_password: string;

  /**
   * 新密码
   */
  @ApiProperty({
    description: '新密码',
  })
  new_password: string;
}
