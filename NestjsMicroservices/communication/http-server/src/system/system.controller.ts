import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Put,
  HttpCode,
  UseGuards,
} from '@nestjs/common';
import { SystemService } from './system.service';
import { Logger } from '@nestjs/common';
import { TokenAuthGuard } from 'src/auth/guard/token.authguard';
import { SkipTokenAuth } from 'src/auth/guard/skip-token-auth.decorator';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@Controller('system')
@ApiTags('字典')
export class SystemController {
  private readonly logger = new Logger(SystemController.name);
  constructor(private readonly systemService: SystemService) {}

  /**
   * 获取字典列表
   * @param type 获取字典类型 默认为system
   * @returns
   */
  @Get()
  @HttpCode(200)
  @SkipTokenAuth()
  @ApiOperation({ summary: '获取字典列表', description: '获取字典列表' })
  getAllDict(@Query('type') type: string) {
    return this.systemService.getAllDict(type);
  }

  /**
   * 获取特定系统字典
   * @param type 获取字典类型 默认为system
   * @returns
   */
  @Get(':name')
  @SkipTokenAuth()
  @ApiOperation({
    summary: '获取特定系统字典',
    description: '获取特定系统字典',
  })
  async getSystemDictRowByName(@Param('name') name: string) {
    return await this.systemService.getSystemDictRowByName(name);
  }

  /**
   * 新建字典
   * @param name 字典英文名
   * @param title 字典中文名
   * @param type 是否封存 默认0不封存 [0 | 1]
   * @param sealed 备注
   * @param comment 字典业务类型 默认system  [ system | business]
   * @returns
   */
  @Post()
  @HttpCode(200)
  create(
    @Body('name') name: string,
    @Body('title') title: string,
    @Body('sealed') sealed: number,
    @Body('comment') comment: string,
    @Body('type') type: string,
  ) {
    // 业务判空
    if (
      name === '' ||
      name === undefined ||
      title === '' ||
      title === undefined
    )
      throw '缺少参数';
    // 类型判断
    switch (type) {
      case 'business':
        break;
      default:
        type = 'system';
        break;
    }
    //新建字典时不需要组合system_dict_前缀,sql语句中已经包含
    //此处的name为字典英文名,不是数据库表名,数据库表名为system_dict_name
    return this.systemService.create(name, title, type, sealed, comment);
  }

  /**
   * 创建字典的ROW
   * @param {*} name 名称
   * @param {*} enumValue 枚举值
   * @param {*} code 代码
   * @param {*} parent_id 父节点ID
   * @param {*} depth 树结构深度
   * @param {*} sorting_id 排序代码
   * @param {*} sealed 封存
   * @param {*} comment 注释
   * @returns
   */
  @Post(':name')
  @HttpCode(200)
  createSystemDictRowByName(
    @Param('name') name: string,
    @Body('enumTitle') enumTitle: string,
    @Body('enumValue') enumValue: string,
    @Body('code') code: string,
    @Body('sorting_id') sorting_id: number,
    @Body('sealed') sealed: number,
    @Body('parent_id') parent_id: number,
    @Body('depth') depth: number,
    @Body('comment') comment: string,
  ) {
    return this.systemService.createSystemDictRowByName(
      name,
      enumTitle,
      enumValue,
      code,
      parent_id,
      depth,
      sorting_id,
      sealed,
      comment,
    );
  }

  /**
   * 通过字典名和字段ID 更新特定字段信息
   * @param {*} name
   * @param {*} rowID
   * @param {*} enumValue
   * @param {*} code
   * @param {*} sorting_id
   * @param {*} sealed
   * @returns
   */
  @Put(':name/:rowID')
  @HttpCode(200)
  updateDictRowByNameAndRowID(
    @Param('name') name: string,
    @Param('rowID') rowID: number,
    @Body('enumTitle') enumTitle: string,
    @Body('enumValue') enumValue: string,
    @Body('code') code: string,
    @Body('sorting_id') sorting_id: number,
    @Body('sealed') sealed: number,
    @Body('comment') comment: string,
  ) {
    return this.systemService.updateDictRowByNameAndRowID(
      name,
      rowID,
      enumTitle,
      enumValue,
      code,
      sorting_id,
      sealed,
      comment,
    );
  }

  /**
   * 通过字典名更新字典
   * @param {*} name 字典英文名
   * @param {*} title 字典中文名
   * @param {*} comment 注释
   * @param {*} sealed 封存
   * @returns
   */
  @Put(':name')
  @HttpCode(200)
  updateDictByName(
    @Param('name') name: string,
    @Body('title') title: string,
    @Body('comment') comment: string,
    @Body('sealed') sealed: number,
  ) {
    return this.systemService.updateDictByName(name, title, comment, sealed);
  }

  /**
   * 删除特定字典
   * @param name 字典名
   */
  @Delete(':name')
  @HttpCode(200)
  deleteSystemDictRowByName(@Param('name') name: string) {
    return this.systemService.deleteSystemDictRowByName(name);
  }

  /**
   * 删除特定字典的特定Row
   * @param name 系统字典的名称
   * @param rowID 上述系统字典的中的RowID
   */
  @Delete(':name/:rowID')
  deleteDictRowByNameAndRowID(
    @Param('name') name: string,
    @Param('rowID') rowID: number,
  ) {
    {
      return this.systemService.deleteDictRowByNameAndRowID(name, rowID);
    }
  }
}
