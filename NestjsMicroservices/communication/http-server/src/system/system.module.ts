import { Module } from '@nestjs/common';
import { SystemService } from './system.service';
import { SystemController } from './system.controller';
import { JwtService } from '@nestjs/jwt';
import { ConfigModule } from 'src/config/config.module';

@Module({
  imports: [ConfigModule],
  controllers: [SystemController],
  providers: [SystemService, JwtService],
})
export class SystemModule {}
