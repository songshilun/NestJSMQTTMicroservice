export class systables {}

export class Equip_type_static {
  equip_type_static_id;
  equip_type_static_name;
}

export class DicSys {
  enumValue;
  enumTitle;
}

export class Weapon_type_static {
  weapon_type_static_id;
  weapon_type_static_name;
}

export class Comprehensive_support_static {
  Comprehensive_support_id;
  Comprehensive_support_name;
}

export class Behaviour_static {
  Behaviour_id;
  Behaviour_name;
}

export class Formation_static {
  Formation_id;
  Formation_name;
}
