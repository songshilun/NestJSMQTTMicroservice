/*
 * @作者: 宋时轮
 * @Date: 2023-03-13 10:01:11
 * @LastEditors: 宋时轮
 * @LastEditTime: 2023-07-07 10:01:11
 * @=: ============================================================
 * @版本: v1.0
 * @描述: 字典服务
 */
import { Injectable, OnModuleInit } from '@nestjs/common';
import { Logger } from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class SystemService implements OnModuleInit {
  private readonly logger = new Logger(SystemService.name);
  constructor(private readonly configService: ConfigService) {}
  private client: ClientProxy;
  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
    this.logger.log('登录服务消息队列通讯初始化完成...');
  }

  /**
   * 获取字典列表
   * @param type 获取字典类型 默认为system
   * @returns
   */
  async getAllDict(type) {
    await this.client.emit(
      'Log',
      `[HTTP微服务]接收到[获取字典列表],请求的信息是:[类型]:${type},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-system-GetAllDict', type);
  }

  /**
   * 新建字典
   * @param name 字典英文名
   * @param title 字典中文名
   * @param type 是否封存 默认0不封存 [0 | 1]
   * @param sealed 备注
   * @param comment 字典业务类型 默认system  [ system | business]
   * @returns
   */
  async create(name, title, type, sealed, comment) {
    const data = {
      name,
      title,
      type,
      sealed,
      comment,
    };
    await this.client.emit(
      'Log',
      `[HTTP微服务]接收到[新建字典],请求的信息是:[数据]:${data},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-system-create', data);
  }

  /**
   * 获取特定系统字典
   * @returns
   */
  async getSystemDictRowByName(name) {
    try {
      await this.client.emit(
        'Log',
        `[HTTP微服务]接收到[字典请求],请求的信息是:[名称]:${name},正在推送至事件中心`,
      );
      return await this.client.send(
        'MessageHub-system-getSystemDictRowByName',
        name,
      );
    } catch (e) {
      throw e;
    }
  }

  /**
   * 创建字典的ROW
   * @param {*} name 名称
   * @param {*} enumTitle 枚举标题
   * @param {*} enumValue 枚举值
   * @param {*} code 代码
   * @param {*} parent_id 父节点ID
   * @param {*} depth 树结构深度
   * @param {*} sorting_id 排序代码
   * @param {*} sealed 封存
   * @param {*} comment 注释
   * @returns
   */
  async createSystemDictRowByName(
    name,
    enumTitle,
    enumValue,
    code,
    parent_id,
    depth,
    sorting_id,
    sealed,
    comment,
  ) {
    const data = {
      name,
      enumTitle,
      enumValue,
      code,
      parent_id,
      depth,
      sorting_id,
      sealed,
      comment,
    };
    await this.client.emit(
      'Log',
      `[HTTP微服务]接收到[创建字典的ROW],请求的信息是:数据:${data},正在推送至事件中心`,
    );
    return await this.client.send(
      'MessageHub-system-createSystemDictRowByName',
      name,
    );
  }

  /**
   * 通过字典名更新字典
   * @param {*} name 字典英文名
   * @param {*} title 字典中文名
   * @param {*} comment 注释
   * @param {*} sealed 封存
   * @returns
   */
  async updateDictByName(name, title, comment, sealed) {
    const data = {
      name,
      title,
      comment,
      sealed,
    };
    await this.client.emit(
      'Log',
      `[HTTP微服务]接收到[通过字典名更新字典],请求的信息是:数据:${data},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-system-updateDictByName', name);
  }

  /**
   * 通过字典名和字段ID 更新特定字段信息
   * @param {*} name
   * @param {*} rowID
   * @param {*} enumValue
   * @param {*} code
   * @param {*} sorting_id
   * @param {*} sealed
   * @returns
   */
  async updateDictRowByNameAndRowID(
    name,
    rowID,
    enumTitle,
    enumValue,
    code,
    sorting_id,
    sealed,
    comment,
  ) {
    const data = {
      name,
      rowID,
      enumTitle,
      enumValue,
      code,
      sorting_id,
      comment,
      sealed,
    };
    await this.client.emit(
      'Log',
      `[HTTP微服务]接收到[通过字典名和字段ID 更新特定字段信息],请求的信息是:数据:${data},正在推送至事件中心`,
    );
    return await this.client.send(
      'MessageHub-system-updateDictRowByNameAndRowID',
      name,
    );
  }

  /**
   * 删除特定字典
   * @param name 字典名
   */
  async deleteSystemDictRowByName(name) {
    await this.client.emit(
      'Log',
      `[HTTP微服务]接收到[删除特定字典],请求的信息是:[名称]:${name},正在推送至事件中心`,
    );
    return await this.client.send(
      'MessageHub-system-deleteSystemDictRowByName',
      name,
    );
  }

  /**
   * 删除特定字典的特定Row
   * @param name 系统字典的名称
   * @param rowID 上述系统字典的中的RowID
   */
  async deleteDictRowByNameAndRowID(name, rowID) {
    const data = {
      name,
      rowID,
    };
    await this.client.emit(
      'Log',
      `[HTTP微服务]接收到[删除特定字典的特定Row],请求的信息是:数据:${data},正在推送至事件中心`,
    );
    return await this.client.send(
      'MessageHub-system-deleteDictRowByNameAndRowID',
      name,
    );
  }
}
