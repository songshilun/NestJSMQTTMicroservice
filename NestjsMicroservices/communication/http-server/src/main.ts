import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as path from 'path';
import * as fs from 'fs';
import { Logger } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { generateDocmment } from './doc';

const logger = new Logger('Main');

async function bootstrap() {
  const rootPath = process.cwd(); // 当前文件的目录
  const configFilePath = path.join(rootPath, 'config.json'); // 完整的配置文件路径
  // const parentDir = path.dirname(rootPath); // 上一级目录
  // const grandParentDir = path.dirname(parentDir); // 上一级的上一级目录

  // 读取配置文件
  const configData = fs.readFileSync(configFilePath, 'utf8');
  let config = JSON.parse(configData);

  const app = await NestFactory.create(AppModule);
  const port = config.Http.port;
  app.enableCors();

  //初始化接口文档系统 swagger和knife4j
  generateDocmment(app);

  await app.listen(port);
  logger.log(`HTTP微服务启动监听,监听端口是${port}`);
}
bootstrap();
