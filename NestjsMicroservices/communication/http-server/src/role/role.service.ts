import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class RoleService {
  private client: ClientProxy;
  private readonly logger = new Logger(RoleService.name);
  constructor(private readonly configService: ConfigService) {}

  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
    this.logger.log('登录服务消息队列通讯初始化完成...');
  }

  async create(createRoleDto: CreateRoleDto) {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[角色新建请求],请求的数据是:[角色名称]${createRoleDto.role_name} [关联权限]${createRoleDto.role_permission},正在推送至事件中心`,
    );
    return await this.client.send<CreateRoleDto>(
      'MessageHub-Role-Create',
      createRoleDto,
    );
  }

  async findAll() {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[角色获取列表请求],正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-Role-FindAll', '');
  }

  async findOne(id: number) {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[角色获取单条数据请求],请求的数据是:[主键ID]:${id},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-Role-FindOne', id);
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[角色更新请求],请求的数据是:[主键ID]:${id},[角色名称]${updateRoleDto.role_name} [关联权限]${updateRoleDto.role_permission},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-Role-Update', {
      id,
      updateRoleDto,
    });
  }

  async remove(id: number) {
    await this.client.emit<string>(
      'Log',
      `[HTTP微服务]接收到[角色删除请求],请求的数据是:[主键ID]:${id},正在推送至事件中心`,
    );
    return await this.client.send('MessageHub-Role-Update', id);
  }
}
