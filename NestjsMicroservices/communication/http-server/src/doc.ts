import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { knife4jSetup } from 'nest-knife4j';
import * as fs from 'fs';
import * as path from 'path';
// 文档
export const generateDocmment = (app: INestApplication) => {
  const rootPath = process.cwd(); // 当前文件的目录
  const configFilePath = path.join(rootPath, 'config.json'); // 完整的配置文件路径
  const configData = fs.readFileSync(configFilePath, 'utf8');
  let config = JSON.parse(configData);

  const DocConfig = new DocumentBuilder()
    .setTitle(config.version.version_title)
    .setDescription(config.version.version_des)
    .setVersion(config.version.version)
    .setExternalDoc('设置作者', '')
    .setLicense('宋时轮', '')
    .build();

  const document = SwaggerModule.createDocument(app, DocConfig);
  SwaggerModule.setup('api', app, document);
  knife4jSetup(app, [
    {
      name: '文件管理系统',
      url: `/api-json`,
      swaggerVersion: config.version.version,
      location: `/api-json`,
    },
  ]);
};
