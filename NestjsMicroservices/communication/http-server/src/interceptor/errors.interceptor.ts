import {
  Catch,
  ExceptionFilter,
  ArgumentsHost,
  HttpStatus,
  Inject,
  Logger,
  UnauthorizedException,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from 'src/config/config.service';
import { QueryFailedError } from 'typeorm';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(GlobalExceptionFilter.name);
  constructor(private readonly configService: ConfigService) {}
  private client: ClientProxy;
  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
  }
  async catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    this.logger.error(exception);
    await this.client.emit<string>(
      'LogError',
      `[HTTP微服务]出现报错,报错的信息是:${exception}`,
    );
    if (exception instanceof NotFoundException) {
      // 404错误
      response.status(HttpStatus.NOT_FOUND).json({
        code: -1,
        timestamp: new Date().toISOString(),
        message: '接口不存在',
      });
      return;
    }

    if (exception instanceof QueryFailedError) {
      // 数据库查询错误
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        code: -1,
        timestamp: new Date().toISOString(),
        message: '数据库执行错误,请检查请求参数或者服务器内部错误',
      });
      return;
    }

    if (exception instanceof UnauthorizedException) {
      // 鉴权类型的异常
      response.status(HttpStatus.OK).json({
        code: -999,
        timestamp: new Date().toISOString(),
        message: 'Token无效或已过期',
      });
      return;
    }

    if (exception instanceof Error) {
      // 其他类型的异常
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        code: -1,
        timestamp: new Date().toISOString(),
        message:
          '接口请求异常或者服务器内部逻辑异常,异常信息:' + exception.message,
      });
      return;
    }

    // 其他类型的异常
    response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      code: -1,
      timestamp: new Date().toISOString(),
      message:
        '接口请求异常或者服务器内部逻辑异常,异常信息:' + exception.message,
    });
    return;
  }
}
