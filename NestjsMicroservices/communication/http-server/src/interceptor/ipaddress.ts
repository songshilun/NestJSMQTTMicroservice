import { ExecutionContext, createParamDecorator } from '@nestjs/common';

export const IpAddress = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {

    const request = ctx.switchToHttp().getRequest();
    let ip =
      request.headers['x-forwarded-for'] || request.connection.remoteAddress;
    //request.connection.remoteAddress 返回的是 IP 地址，
    //但在某些情况下，IPv4 地址可能会被包裹在 IPv6 地址中，
    //以 ::ffff: 开头。这是因为在 IPv6 环境中，IPv4 地址被映射成 IPv6 地址的一部分。
    //所以增加了去除这部分的代码
    if (ip && ip.startsWith('::ffff:')) {
      ip = ip.slice(7);
    }
    const port = request.connection.remotePort;
    const ip_address_port = ip + ':' + port;
    return ip_address_port;
  },
);
