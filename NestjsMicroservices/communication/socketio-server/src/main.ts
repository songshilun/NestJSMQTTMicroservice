import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as path from 'path';
import { Transport } from '@nestjs/microservices';
import * as fs from 'fs';
import { Logger } from '@nestjs/common';

const logger = new Logger('SocketIO微服务');

async function bootstrap() {
  const rootPath = process.cwd(); // 当前文件的目录
  const configFilePath = path.join(rootPath, 'config.json'); // 完整的配置文件路径
  // const parentDir = path.dirname(rootPath); // 上一级目录
  // const grandParentDir = path.dirname(parentDir); // 上一级的上一级目录

  // 读取配置文件
  const configData = fs.readFileSync(configFilePath, 'utf8');
  const config = JSON.parse(configData);
  const microServiceOptions = {
    transport: Transport.MQTT,
    options: {
      host: config.MQTT.host,
      port: config.MQTT.port,
    },
  };
  const app = await NestFactory.createMicroservice(
    AppModule,
    microServiceOptions,
  );
  // 注意：这里我们不会使用 RMQ 或其他传输层，因为我们只想使用 Socket.IO
  // 因此，我们需要为 Socket.IO 创建一个适配器
  //app.useWebSocketAdapter(new SocketIoAdapter(app));
  app.listen();
  logger.log(`SocketIO微服务已经上线`);
  // const app = await NestFactory.create(AppModule);
  // await app.listen(4501);
}
bootstrap();
