import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SocketioModule } from './socketio/socketio.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MqttService } from './service/mqtt/mqtt.service';
import { MqttModule } from './service/mqtt/mqtt.module';

@Module({
  imports: [
    ConfigModule,
    MqttModule,
    SocketioModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.getDatabaseConfig().host,
        port: configService.getDatabaseConfig().port,
        username: configService.getDatabaseConfig().username,
        password: configService.getDatabaseConfig().password,
        database: configService.getDatabaseConfig().databaseName,
        entities: [__dirname + '/**/*.entity{.ts,.js}'], // 实体类文件所在位置，自动加载实体
        synchronize: configService.getDatabaseConfig().synchronize, // 自动创建表
        logging: configService.getDatabaseConfig().logging, // 打印日志
        // 其他连接选项...
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
