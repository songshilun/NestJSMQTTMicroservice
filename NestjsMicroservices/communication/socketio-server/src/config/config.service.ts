import { Injectable, Logger } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';

/**
 * 配置服务
 */
@Injectable()
export class ConfigService {
  private readonly logger = new Logger(ConfigService.name);
  private readonly config: Record<string, any>;

  constructor() {
    const rootPath = process.cwd(); // 当前文件的目录
    const configFilePath = path.join(rootPath, 'config.json'); // 完整的配置文件路径
    // const parentDir = path.dirname(rootPath); // 上一级目录
    // const grandParentDir = path.dirname(parentDir); // 上一级的上一级目录

    try {
      // 读取配置文件
      const configData = fs.readFileSync(configFilePath, 'utf8');
      this.config = JSON.parse(configData);
    } catch (error) {
      throw new Error(`无法读取配置文件请确认: ${error.message}`);
    }
  }

  get(key: string): any {
    // 获取配置选项
    return this.config[key];
  }

  getMQTTConfig(): any {
    // 获取数据库配置
    return this.config.MQTT;
  }

  getJwtConfig(): any {
    // 获取JWT配置
    return this.config.jwt;
  }

  getDatabaseConfig(): any {
    // 获取数据库配置
    return this.config.database;
  }

  getTemplateConfig(): any {
    // 获取模版配置
    return this.config.template;
  }
}
