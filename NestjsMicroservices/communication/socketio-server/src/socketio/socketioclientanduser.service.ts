import { Injectable } from '@nestjs/common';
import { CreateSocketioDto } from './dto/create-socketio.dto';
import { UpdateSocketioDto } from './dto/update-socketio.dto';
import { Socket } from 'socket.io';
import { ClientModel } from './model/socketio.model';
import { user } from './dto/user.entity';

@Injectable()
export class SocketioClientAndUserService {
  private clients: Map<string, ClientModel> = new Map();

  /**
   * 添加连接
   * @param id 连接ID
   * @param client 连接体
   */
  addClient(id: string, socket: Socket, user: user): void {
    const TempClientModel = new ClientModel();
    TempClientModel.have_user = true;
    TempClientModel.socket = socket;
    TempClientModel.user = user;
    this.clients.set(id, TempClientModel);
  }

  /**
   * 移除连接
   * @param id 连接ID
   */
  removeClient(id: string): void {
    this.clients.delete(id);
  }

  /**
   * 获取连接
   * @param id 连接ID
   * @returns 返回连接体
   */
  getClient(id: string): Socket | undefined {
    return this.clients.get(id).socket;
  }

  /**
   * 向单个客户端推送事件+数据
   * @param id 客户端ID
   * @param event 事件
   * @param data 数据
   */
  emitToClient(id: string, event: string, data: any): void {
    const client = this.getClient(id);
    if (client) {
      client.emit(event, data);
    }
  }

  broadcast(event: string, data: any): void {
    this.clients.forEach((client) => {
      client.socket.emit(event, data);
    });
  }
}
