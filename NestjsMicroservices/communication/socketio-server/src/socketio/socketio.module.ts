import { Module } from '@nestjs/common';
import { SocketioClientService } from './socketioclient.service';
import { SocketioGateway } from './socketio.gateway';
import { SocketioGroupService } from './socketioroom.service';
import { SocketIOService } from './socketio.service';
import { ConfigModule } from 'src/config/config.module';
import { MqttModule } from 'src/service/mqtt/mqtt.module';

@Module({
  imports: [ConfigModule, MqttModule],
  providers: [
    SocketioGateway,
    SocketIOService,
    SocketioClientService,
    SocketioGroupService,
  ],
})
export class SocketioModule {}
