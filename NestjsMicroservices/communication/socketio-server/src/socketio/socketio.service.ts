import { Injectable, Logger } from '@nestjs/common';
import { CreateSocketioDto, StrData } from './dto/create-socketio.dto';
import { UpdateSocketioDto } from './dto/update-socketio.dto';
import { Socket } from 'socket.io';
import { SocketioClientService } from './socketioclient.service';
import { SocketioGroupService } from './socketioroom.service';
import { EntityManager } from 'typeorm';
import { ConfigService } from 'src/config/config.service';
// import { modeldata } from 'src/resource/model/entities/model.entity';
// import { CreateModel_structureDto } from 'src/model_structure/dto/create-model_structure.dto';
// import { Model_str_compar } from 'src/model_str_compar/entities/model_str_compar.entity';
// import { Model_structure } from 'src/model_structure/entities/model_structure.entity';

@Injectable()
export class SocketIOService {
  private readonly logger = new Logger(SocketIOService.name);
  private CenterSocket: Socket;
  constructor(
    private manager: EntityManager,
    private readonly ConfigService: ConfigService,
    public readonly SocketIOClientService: SocketioClientService,
    public readonly SocketioGroupService: SocketioGroupService,
  ) {}

  /**
   * 接收客户端需要向多个客户端推送数据
   * @param ClientsGroupSend 推送数据结构体 内部包括 需要推送多个客户端的ID集合 推送的事件 以及需要推送的数据
   * @returns
   */
  ClientsSend(ClientsSendData: string) {
    this.logger.log('接收到的消息是' + ClientsSendData);
    const ClientsSendTempData = JSON.parse(ClientsSendData);
    const ClientIDs = ClientsSendTempData.clientIDs;
    const eventName = ClientsSendTempData.eventName;
    const Have_obj = ClientsSendTempData.Have_obj;
    const obj = ClientsSendTempData.obj;
    const Send_Center = ClientsSendTempData.Send_Center;
    for (const clientID of ClientIDs) {
      const Client = this.SocketIOClientService.getClient(clientID);
      if (Have_obj) {
        if (Send_Center) {
          if (this.CenterSocket != null) {
            this.CenterSocket.emit(eventName, obj);
          }
        } else {
          Client.emit(eventName, obj);
        }
      } else {
        if (Send_Center) {
          if (this.CenterSocket != null) {
            this.CenterSocket.emit(eventName);
          }
        } else {
          Client.emit(eventName);
        }
      }
    }
    return '';
  }

  /**
   * 接收客户端需要想组推送数据
   * @param ClientsGroupSend 推送数据结构体 内部包括 需要推送的组 推送的事件 以及需要推送的数据
   * @returns
   */
  GroupSend(ClientsGroupSend: string) {
    this.logger.log('接收到的消息是' + ClientsGroupSend);
    const ClientsSendTempData = JSON.parse(ClientsGroupSend);
    const groupName = ClientsSendTempData.groupName;
    const eventName = ClientsSendTempData.eventName;
    const obj = ClientsSendTempData.obj;
    const Have_obj = ClientsSendTempData.Have_obj;
    const Send_Center = ClientsSendTempData.Send_Center;
    this.SocketioGroupService.broadcastToRoom(
      groupName,
      eventName,
      obj,
      Have_obj,
    );
    return;
  }

  CenterLogin(client: Socket) {
    this.CenterSocket = client;
    this.logger.log(`中心机上线,中心机ID已覆写,中心机ID是:${client.id}`);
    return '';
  }

  // /**
  //  * 模型解析结果入库
  //  * @param DealWithModelResultData
  //  */
  // async DealWithModelResult(DealWithModelResultData: string) {
  //   const TempData = JSON.parse(DealWithModelResultData);
  //   const model_code: number = TempData.model_code;
  //   const StrData: StrData = TempData.StrData;
  //   const model_data = await this.manager.findOne(modeldata, {
  //     where: {
  //       model_code: model_code,
  //     },
  //   });
  //   if (this.ConfigService.getAnalysisConfig().isDeleteOldData == true) {
  //     if (model_data.analytic_state == 3 || model_data.analytic_state == 4) {
  //       const temp_model_strs = await this.manager.find(Model_structure, {
  //         where: {
  //           ass_model_code: model_data.model_code,
  //         },
  //       });
  //       for (const temp_model_str of temp_model_strs) {
  //         await this.manager.delete(Model_structure, temp_model_str.id);
  //       }
  //     }
  //   }

  //   //this.logger.log(TempData);

  //   try {
  //     let errormsg: string[] = [];
  //     //判空检查
  //     if (model_data == null || model_data == undefined) {
  //       this.logger.error('解析数据异常,请确认模型数据是否被删除');
  //       return;
  //     }
  //     await this.insertNodeRecursive(StrData, model_code, errormsg, -1);
  //     model_data.errormsg = JSON.stringify(errormsg);
  //     //解析成功
  //     model_data.analytic_state = 3;
  //     await this.manager.save(modeldata, model_data);
  //   } catch (error) {
  //     //解析失败,更改状态
  //     model_data.analytic_state = 4;
  //     await this.manager.save(modeldata, model_data);
  //     this.logger.error(error);
  //   }
  // }

  // private async insertNodeRecursive(
  //   data: StrData,
  //   model_code: number,
  //   errormsg: string[],
  //   parentId?: number,
  // ) {
  //   const model_str = new CreateModel_structureDto();

  //   model_str.parent_str_encode = parentId;
  //   model_str.ass_model_code = model_code;
  //   // const model_str_compars = await this.manager.find(Model_str_compar, {
  //   //   where: {
  //   //     nick_name: data.name,
  //   //   },
  //   // });

  //   const model_str_compars = await this.manager
  //     .createQueryBuilder(Model_str_compar, 'compar')
  //     .where('compar.ass_model_id LIKE :value', {
  //       value: `%${model_code}%`,
  //     })
  //     .where('compar.nick_name = :nick_name', { nick_name: data.name })
  //     .getMany();

  //   model_str.str_compera_id = -1;
  //   for (const model_str_compar of model_str_compars) {
  //     const ass_model_ids = model_str_compar.ass_model_id.split(',');
  //     if (ass_model_ids.includes(model_code.toString())) {
  //       model_str.is_show = model_str_compar.is_show;
  //       model_str.str_compera_id = model_str_compar.id;
  //     }
  //   }
  //   if (model_str.str_compera_id == -1) {
  //     errormsg.push(data.name);
  //     this.logger.error(
  //       `无法在模型结构对照表内查询到数据,需要查询的名字是:${data.name}`,
  //     );
  //   }

  //   let model_str_result = await this.manager.findOne(Model_structure, {
  //     where: {
  //       str_compera_id: model_str.str_compera_id,
  //     },
  //   });

  //   if (this.ConfigService.getAnalysisConfig().isDeleteOldData == true) {
  //     model_str_result = await this.manager.save(Model_structure, model_str);
  //   } else {
  //     if (model_str_result != undefined && model_str_result != null) {
  //       model_str_result.str_compera_id = model_str.str_compera_id;
  //       model_str_result.parent_str_encode = model_str.parent_str_encode;
  //       await this.manager.save(Model_structure, model_str_result);
  //     } else {
  //       model_str_result = await this.manager.save(Model_structure, model_str);
  //     }
  //   }

  //   for (const child of data.children || []) {
  //     await this.insertNodeRecursive(
  //       child,
  //       model_code,
  //       errormsg,
  //       model_str_result.id,
  //     );
  //   }
  // }
}
