﻿import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
  ConnectedSocket,
} from '@nestjs/websockets';
import { SocketioClientService } from './socketioclient.service';
import { Server, Socket } from 'socket.io';
import { Logger } from '@nestjs/common';
import { SocketIOService } from './socketio.service';
import { MqttService } from 'src/service/mqtt/mqtt.service';
import { ClientNats } from '@nestjs/microservices';
import { timeInterval, timeout } from 'rxjs';
import { LoginStateChange } from './dto/create-socketio.dto';

@WebSocketGateway(4501, {
  allowEIO3: true, //协议前后端版本要一致
  //后端解决跨域
  cors: {
    origin: `*`,
    credentials: true,
  },
})
export class SocketioGateway
  implements OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  server: Server;
  private readonly logger = new Logger(SocketioGateway.name);
  constructor(
    private readonly SocketIOService: SocketIOService,
    private readonly mqttService: MqttService,
  ) {}

  //连接事件
  handleConnection(client: Socket) {
    const clientId = client.id; // 获取连接的 ID
    this.logger.log(`客户端连接接入,连接ID是:${clientId}`);
    // this.SocketIOService.SocketIORoomService.createRoom('Room');
    // this.SocketIOService.SocketIORoomService.joinRoom('Room', client);
    this.SocketIOService.SocketIOClientService.addClient(clientId, client);
  }

  // 在断开连接时触发的事件
  handleDisconnect(client: Socket) {
    const clientId = client.id; // 获取断开连接的 ID
    this.logger.log(`客户端断开,断开的客户端ID是: ${clientId}`);
    this.SocketIOService.SocketIOClientService.removeClient(client.id);
  }

  @SubscribeMessage('ClientsSend')
  ClientsSend(@MessageBody() SendToClient: string) {
    return this.SocketIOService.ClientsSend(SendToClient);
  }

  @SubscribeMessage('GroupSend')
  GroupSend(@MessageBody() GroupSend: string) {
    return this.SocketIOService.GroupSend(GroupSend);
  }

  @SubscribeMessage('CenterLogin')
  CenterLogin(@ConnectedSocket() client: Socket) {
    return this.SocketIOService.CenterLogin(client);
  }

  @SubscribeMessage('managerLogin')
  ManagerLogin(@ConnectedSocket() client: Socket, @MessageBody() Data) {
    this.logger.log(Data);
    const data = Data;

    //this.mqttService.emit('LoginSuccess', data.userId);

    setTimeout(() => {
      client.emit('fresh', '123');
    }, 5000);

    //构建登录状态变更结构体并推送到队列内
    const loginstatechange = new LoginStateChange();
    loginstatechange.userid = data.userId;
    loginstatechange.state = true;
    this.mqttService.emit('LoginStateChange', loginstatechange);

    return client.emit('loginSuccess', client.id);
  }

  @SubscribeMessage('managerLogout')
  ManagerLogout(@ConnectedSocket() client: Socket) {
    return client.emit('logoutSuccess', '退出成功');
  }

  // @SubscribeMessage('DealWithModelResult')
  // DealWithModelResult(
  //   @ConnectedSocket() client: Socket,
  //   @MessageBody() DealWithModelResult: string,
  // ) {
  //   return this.SocketIOService.DealWithModelResult(DealWithModelResult);
  // }
}
