import { Socket } from 'socket.io';
import { user } from '../dto/user.entity';
export class ClientModel {
  public socket: Socket;
  public have_user: boolean;
  public user: user;
}
