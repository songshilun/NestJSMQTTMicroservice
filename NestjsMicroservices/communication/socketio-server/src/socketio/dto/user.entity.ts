import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity(
    {
        name: 'user'
    }
)
export class user{

    @PrimaryGeneratedColumn({
        comment: '用户ID'
    })
    ID: number

    @Column({
        comment: '用户名'
    })
    username: string

    @Column({
        comment: '密码'
    })
    password: string

    @Column({
        comment: '昵称'
    })
    nickname: string

    @Column({
        comment: '用户权限'
    })
    role: string

    @Column({
        comment: '创建时间'
    })
    create_time: Date
}

