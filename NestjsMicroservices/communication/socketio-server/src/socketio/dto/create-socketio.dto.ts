import { ApiProperty } from '@nestjs/swagger';

export class CreateSocketioDto {
  public id: number;
  public name: string;
}

/// <summary>
/// 结构数据
/// </summary>
export class StrData {
  public name: string;
  public children: StrData[];
}

export class LoginStateChange {
  @ApiProperty({
    description: '用户id',
  })
  userid: number;

  @ApiProperty({
    description: '登录状态变更',
  })
  state: boolean;
}
