import { Test, TestingModule } from '@nestjs/testing';
import { SocketioGateway } from './socketio.gateway';
import { SocketioClientService } from './socketioclient.service';

describe('SocketioGateway', () => {
  let gateway: SocketioGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SocketioGateway, SocketioClientService],
    }).compile();

    gateway = module.get<SocketioGateway>(SocketioGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
