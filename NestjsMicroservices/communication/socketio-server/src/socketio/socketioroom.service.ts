import { Injectable } from '@nestjs/common';
import { Server, Socket } from 'socket.io';
import { WebSocketServer } from '@nestjs/websockets';
@Injectable()
export class SocketioGroupService {
  private groups: Map<string, Set<Socket>> = new Map();

  @WebSocketServer()
  server: Server;

  /**
   * 创建组
   * @param groupName 组名
   */
  createGroup(groupName: string): void {
    if (!this.groups.has(groupName)) {
      this.groups.set(groupName, new Set());
    }
  }

  /**
   * 删除组
   * @param groupName 组名 
   */
  deleteGroup(groupName: string): void {
    if (!this.groups.has(groupName)) {
      this.groups.delete(groupName);
    }
  }

  /**
   * 加入组
   * @param groupName 组名 
   * @param client 客户端连接
   */
  joinRoom(groupName: string, client: Socket): void {
    if (this.groups.has(groupName)) {
      const room = this.groups.get(groupName);
      //console.log(`已将${client.id}加入${roomName}`);
      room.add(client);
      client.join(groupName);
    }
  }

  /**
   * 离开组
   * @param groupName 组名 
   * @param client 客户端连接
   */
  leaveRoom(groupName: string, client: Socket): void {
    if (this.groups.has(groupName)) {
      const room = this.groups.get(groupName);
      room.delete(client);
      client.leave(groupName);
    }
  }

  /**
   * 向组内推送
   * @param groupName 组名 
   * @param event 事件
   * @param data 数据
   * @param Have_obj 是否携带数据
   */
  broadcastToRoom(groupName: string, event: string, data: any,Have_obj:any): void {
    const room = this.groups.get(groupName);
    console.log(room);
    if (room) {
      //this.server.to(groupName).emit(event, data);
        room.forEach((client) => {
          //console.log(`向组内${client.id}推送数据`);
          if(Have_obj){
            client.emit(event, data);
          }else{
            client.emit(event);
          }
        });
    }
  }
}
