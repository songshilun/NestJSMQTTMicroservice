import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';

@Module({
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.getJwtConfig().secret, // 获取 JWT 密钥
        signOptions: { expiresIn: configService.getJwtConfig().expiresIn }, // 获取 JWT 过期时间
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [AuthService,ConfigService],
  exports: [AuthService],
})
export class AuthModule {}
