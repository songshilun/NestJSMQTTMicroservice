import {
  Catch,
  ExceptionFilter,
  ArgumentsHost,
  HttpStatus,
  Inject,
  Logger,
  UnauthorizedException,
  NotFoundException,
} from '@nestjs/common';
import { QueryFailedError } from 'typeorm';

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  private readonly logger = new Logger(GlobalExceptionFilter.name);
  constructor() {}
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    this.logger.error(exception);
    if (exception instanceof NotFoundException) {
      // 404错误
      response.status(HttpStatus.NOT_FOUND).json({
        code: -1,
        timestamp: new Date().toISOString(),
        message: '接口不存在',
      });
      return;
    }

    if (exception instanceof QueryFailedError) {
      // 数据库查询错误
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        code: -1,
        timestamp: new Date().toISOString(),
        message: '数据库执行错误,请检查请求参数或者服务器内部错误',
      });
      return;
    }

    if (exception instanceof UnauthorizedException) {
      // 鉴权类型的异常
      response.status(HttpStatus.OK).json({
        code: -999,
        timestamp: new Date().toISOString(),
        message: 'Token无效或已过期',
      });
      return;
    }

    if (exception instanceof Error) {
      // 其他类型的异常
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
        code: -1,
        timestamp: new Date().toISOString(),
        message:
          '接口请求异常或者服务器内部逻辑异常,异常信息:' + exception.message,
      });
      return;
    }

    // 其他类型的异常
    response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      code: -1,
      timestamp: new Date().toISOString(),
      message:
        '接口请求异常或者服务器内部逻辑异常,异常信息:' + exception.message,
    });
    return;
  }
}
