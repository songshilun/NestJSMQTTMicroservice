import { SetMetadata } from '@nestjs/common';

/**
 * 跳过统一拦截器
 * @returns 
 */
export const NoTransformInterceptor = () => SetMetadata('noTransformInterceptor', true);