/* transform.interceptor.ts */

import {
  Injectable,
  NestInterceptor,
  CallHandler,
  ExecutionContext,
  HttpStatus,
  Logger,
} from '@nestjs/common';
import {
  // ... 其他引入
  Reflector,
} from '@nestjs/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
// import { controlData } from 'src/control/entities/control.entity';
// import { CreateControlDto } from 'src/control/dto/create-control.dto';

/**
 * 统一返回格式 拦截器 并将操作日志写入数据库
 */
@Injectable()
export class TransformInterceptor implements NestInterceptor {
  private readonly logger = new Logger(TransformInterceptor.name);
  constructor(
    @InjectEntityManager()
    private manager: EntityManager,
    private readonly reflector: Reflector,
  ) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    // // // 从元数据中获取 NoTransformInterceptor 装饰器
    // const noTransformInterceptor = this.reflector.get<boolean>(
    //   'noTransformInterceptor',
    //   context.getHandler(),
    // );

    // if (noTransformInterceptor != null && noTransformInterceptor != undefined) {
    //   // 如果存在标识，直接跳过拦截器逻辑
    //   return next.handle();
    // }
    return next.handle().pipe(
      map((data) => {
        context.switchToHttp().getResponse().status(HttpStatus.OK);
        const request = context.switchToHttp().getRequest();
        const route = request.route?.path || '';
        const method = request.method;
        const requestContent = request.body || request.query;
        let action = '';

        switch (method) {
          case 'GET':
            action = '获取数据';
            break;
          case 'POST':
            action = '提交数据';
            break;
          case 'PUT':
            action = '更新数据';
            break;
          case 'DELETE':
            action = '删除数据';
            break;
          case 'PATCH':
            action = '更新数据';
            break;
          // 根据需要添加其他请求方法的转换
          default:
            action = method; // 默认使用请求方法作为操作类型
            break;
        }
        if (data.controls) {
          //判断是否有Type字段，有则使用Type字段作为操作类型
          if (data.controls.Type != undefined) {
            action = data.controls.Type;
          }
          // const ControlDto = new CreateControlDto();
          // ControlDto.route = route;
          // ControlDto.action = action;
          // ControlDto.request = JSON.stringify(requestContent);
          // ControlDto.response = JSON.stringify(data);
          // ControlDto.change_at = data.controls.change_at;
          // ControlDto.modify_user = data.controls.user;
          // ControlDto.handleid = data.controls.handleid;
          // ControlDto.content = JSON.stringify(data.controls.content);
          // ControlDto.title = data.controls.title;
          //this.logger.log(ControlDto.handleid);
          // this.logger.log('\nroute:\n' +  ControlDto.route + '\naction:\n' + ControlDto.action
          // + '\nrequest:\n' + ControlDto.request + '\nresponse:\n' + ControlDto.response
          // + '\nchange_at:\n' + ControlDto.change_at + '\nuser:\n' + ControlDto.modify_user
          // + '\nhandleid:\n' + ControlDto.handleid + '\ncontent:\n' + ControlDto.content
          // + '\ntilte:\n' + ControlDto.title + ' ');

          // // 将操作日志写入数据库
          // this.manager.save(controlData, {
          //   route: ControlDto.route,
          //   action: ControlDto.action,
          //   request: ControlDto.request,
          //   response: ControlDto.response,
          //   change_at: new Date(),
          //   modify_user: data.controls.modify_user,
          //   handleid: data.controls.handleid,
          //   content: ControlDto.content,
          //   title: ControlDto.title,
          // });
        }

        return {
          data: data.data,
          code: data.code || 1,
          msg: data.msg,
        };
      }),
    );
  }
}
