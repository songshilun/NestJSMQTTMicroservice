// request-logger.middleware.ts

import { Injectable, Logger, NestMiddleware, Req } from '@nestjs/common';
import express, { Request, Response, NextFunction, json } from 'express';

@Injectable()
export class RequestLoggerMiddleware implements NestMiddleware {
  private readonly logger = new Logger(RequestLoggerMiddleware.name);
  use(@Req() req: Request, res: Response, next: NextFunction) {
    //记录请求信息;

    // this.logger.debug('=================================================');
    // this.logger.log(`Request URL: ${req.originalUrl}`);
    // this.logger.log(`Request Method: ${req.method}`);
    // this.logger.log(`Request Headers: ${JSON.stringify(req.headers)}`);
    // this.logger.log(`Request Body: ${JSON.stringify(req.body)}`);
    // this.logger.debug('====================================-=============');

    // if (req.headers['content-type']?.includes('multipart/form-data')) {
    //   // 如果是 FormData 请求体，将其打印为字符串
    //   let formDataString = '';

    //   req.on('data', (chunk) => {
    //     formDataString += chunk.toString();
    //   });

    //   req.on('end', () => {
    //     // 打印 FormData 请求体
    //     this.logger.log('FormData Request Body:', formDataString);
    //     next();
    //   });
    // }
    next();
  }
}
