import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { TokenAuthGuard } from './auth/guard/token.authguard';
import { GlobalExceptionFilter } from './interceptor/errors.interceptor';
import { TransformInterceptor } from './interceptor/transform.interceptor';
import { ExcelModule } from './excel/excel.module';

@Module({
  imports: [ExcelModule],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: GlobalExceptionFilter,
    },
    {
      provide: APP_GUARD,
      useClass: TokenAuthGuard,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule {}
