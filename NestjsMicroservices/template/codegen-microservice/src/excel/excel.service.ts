import { Injectable, Logger, UploadedFile } from '@nestjs/common';
import { CreateExcelDto } from './dto/create-excel.dto';
import { UpdateExcelDto } from './dto/update-excel.dto';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';
import * as XLSX from 'xlsx';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class ExcelService {
  private readonly Logger = new Logger(ExcelService.name);
  constructor(
    @InjectEntityManager()
    private manager: EntityManager,
  ) {}
  create(createExcelDto: CreateExcelDto) {
    return 'This action adds a new excel';
  }

  findAll() {
    return `This action returns all excel`;
  }

  findOne(id: number) {
    return `This action returns a #${id} excel`;
  }

  update(id: number, updateExcelDto: UpdateExcelDto) {
    return `This action updates a #${id} excel`;
  }

  remove(id: number) {
    return `This action removes a #${id} excel`;
  }

  async CreateTable(file: Express.Multer.File) {
    try {
      //存储目标模版文件路径
      const rootPath = process.cwd(); // 当前文件的目录
      const parentDir = path.dirname(rootPath); // 上一级目录
      const grandParentDir = path.dirname(parentDir); // 上一级的上一级目录
      const templatePath = path.join(grandParentDir, 'template'); // 完整的配置文件路径

      //读取表格
      const workbook = XLSX.read(file.buffer);
      const sheetNames: string[] = workbook.SheetNames;
      sheetNames.forEach(async (sheetTempName) => {
        const sheetName = sheetTempName.split('.')[0];
        const Comment = sheetTempName.split('.')[1];
        const worksheet = workbook.Sheets[sheetTempName];
        const range = XLSX.utils.decode_range(worksheet['!ref']);
        const fields = [];
        const primaryKeys = [];
        const haveFiles = [];
        const entityName = this.capitalizeFirstLetter(sheetName);
        for (let C = range.s.c; C <= range.e.c; ++C) {
          //字段名
          const filedName =
            worksheet[XLSX.utils.encode_cell({ r: 0, c: C })]?.v.toString();
          //字段类型
          const filedType =
            worksheet[XLSX.utils.encode_cell({ r: 1, c: C })]?.v.toString();
          //字段描述
          const filedComment =
            worksheet[XLSX.utils.encode_cell({ r: 2, c: C })]?.v.toString();
          //是否为主键
          const isPrimaryKey =
            worksheet[XLSX.utils.encode_cell({ r: 3, c: C })]?.v.toString();
          //是否为包含文件
          const haveFile =
            worksheet[XLSX.utils.encode_cell({ r: 4, c: C })]?.v.toString();

          const fieldDefinition = `${filedName} ${this.sqlFieldType(
            filedType,
          )} COMMENT '${filedComment}'`;
          //将创建数据推入数组
          fields.push(fieldDefinition);

          //判断遍历到的是否为主键
          if (isPrimaryKey === 'yes') {
            primaryKeys.push(filedName);
          }

          //判断遍历到的是否包含文件
          if (haveFile == 'yes') {
            haveFiles.push(filedName);
          }
        }

        //构建新建SQL语句
        const createTalesSQL = `CREATE TABLE temp_${sheetName} (${fields.join(
          ', ',
        )}${
          primaryKeys.length
            ? `, PRIMARY KEY (${primaryKeys.join(',')})) COMMENT = '${Comment}'`
            : ''
        }`;

        const entityFields = [];
        const dtoFields = [];

        fields.map((field) => {
          //解出对应需要的数据
          const [fieldName, filedType, Comment, filedComment] =
            field.split(' ');
          // const filedComment = filedComments
          //   .toString()
          //   .substring(1, filedComments.toString().length - 1);
          //是否包含主键
          const isFieldPrimary = primaryKeys.includes(fieldName);
          //构建主键和字段头标识
          const decorator = isFieldPrimary
            ? `  @PrimaryGeneratedColumn(({
  comment: ${filedComment},
}))`
            : `  @Column({
  comment: ${filedComment},
})`;
          //api标识
          const swagger = `@ApiProperty({
            description: ${filedComment},
            default : ''
          })`;

          entityFields.push(
            `${decorator}
    ${fieldName}: ${this.dtoFieldType(filedType)};\n`,
          );
          dtoFields.push(
            `${swagger}
            ${fieldName}: ${this.dtoFieldType(filedType)};\n`,
          );
        });

        //构建实体
        const entityTemplate = this.createentityTemplate(
          sheetName,
          entityFields,
        );
        //构建新建dto
        const createdto = this.createcreatedto(sheetName, dtoFields);
        //构建更新dto
        const updatedto = this.createupdatedto(sheetName);
        //构建控制器spec
        const controllerspec = this.createcontrollerspec(sheetName);
        //构建控制器
        const controllers = this.createController(sheetName, Comment);
        //构建module
        const module = this.createModule(sheetName);
        //构建服务spec
        const servicespec = this.createServicespec(sheetName);
        //构建服务
        const service = this.createService(sheetName, primaryKeys, entityName);

        this.Logger.log(entityTemplate);
        // //打印SQL语句
        this.Logger.log(createTalesSQL);

        if (!fs.existsSync(`${templatePath}${sheetName}`)) {
          fs.mkdirSync(`./entity_templates/${sheetName}`, {
            recursive: true,
          });
        }
        if (!fs.existsSync(`./entity_templates/${sheetName}/entities/`)) {
          fs.mkdirSync(`./entity_templates/${sheetName}/entities/`, {
            recursive: true,
          });
        }
        if (!fs.existsSync(`./entity_templates/${sheetName}/dto/`)) {
          fs.mkdirSync(`./entity_templates/${sheetName}/dto/`, {
            recursive: true,
          });
        }

        //存储实体
        this.saveEntityTemplateToFile(entityTemplate, sheetName);
        //存储新建dto
        this.saveCreateTemplateToFile(createdto, sheetName);
        //存储更新dto
        this.saveUpdateTemplateToFile(updatedto, sheetName);
        //存储controllerspec
        this.saveControllerspecTemplateToFile(controllerspec, sheetName);
        //存储controller
        this.saveControllerTemplateToFile(controllers, sheetName);
        //存储module
        this.saveModeleTemplateToFile(module, sheetName);
        //存储servicespec
        this.saveServiceSpecTemplateToFile(servicespec, sheetName);
        //存储service
        this.saveServiceTemplateToFile(service, sheetName);

        await this.manager.query(createTalesSQL);
      });
    } catch (error) {
      this.Logger.error(error);
    }

    return '';
  }

  /**
   * 创建Entity
   * @param sheetName
   * @param entityFields
   * @returns
   */
  createentityTemplate(sheetName, entityFields): string {
    return `import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';


    @Entity({
      name: 'repair_${sheetName}',
    })
    export class ${this.capitalizeFirstLetter(sheetName)} {
    ${entityFields.join('\n')}
    }`;
  }

  /**
   * 创建新建DTO
   * @param sheetName
   */
  createcreatedto(sheetName, dtoFields): string {
    return `import { ApiProperty } from '@nestjs/swagger';
    export class Create${this.capitalizeFirstLetter(sheetName)}Dto {
${dtoFields.join('\n')}
}`;
  }

  /**
   * 创建更新DTO
   * @param sheetName
   */
  createupdatedto(sheetName): string {
    //构建更新dto
    return `import { PartialType } from '@nestjs/swagger';
     import { Create${this.capitalizeFirstLetter(
       sheetName,
     )}Dto } from './create-${this.capitalizeFirstLetter(sheetName)}.dto';
           
     export class Update${this.capitalizeFirstLetter(
       sheetName,
     )}Dto extends PartialType(Create${this.capitalizeFirstLetter(
      sheetName,
    )}Dto) {}`;
  }

  /**
   * 创建控制器spec
   * @param sheetName
   * @returns
   */
  createcontrollerspec(sheetName): string {
    return `import { Test, TestingModule } from '@nestjs/testing';
    import { ${this.capitalizeFirstLetter(
      sheetName,
    )}Controller } from './${sheetName}.controller';
    import { ${this.capitalizeFirstLetter(
      sheetName,
    )}Service } from './${sheetName}.service';
    
    describe('${this.capitalizeFirstLetter(sheetName)}Controller', () => {
      let controller: ${this.capitalizeFirstLetter(sheetName)}Controller;
    
      beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
          controllers: [${this.capitalizeFirstLetter(sheetName)}Controller],
          providers: [${this.capitalizeFirstLetter(sheetName)}Service],
        }).compile();
    
        controller = module.get<${this.capitalizeFirstLetter(
          sheetName,
        )}Controller>(${this.capitalizeFirstLetter(sheetName)}Controller);
      });
    
      it('should be defined', () => {
        expect(controller).toBeDefined();
      });
    });`;
  }

  /**
   * 创建控制器
   * @param sheetName
   * @returns
   */
  createController(sheetName, Comment): string {
    //更新controller
    return `import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UploadedFile,
  UseInterceptors,
  Req
} from '@nestjs/common';
import { ${this.capitalizeFirstLetter(
      sheetName,
    )}Service } from './${sheetName}.service';
import { Create${this.capitalizeFirstLetter(
      sheetName,
    )}Dto } from './dto/create-${sheetName}.dto';
import { Update${this.capitalizeFirstLetter(
      sheetName,
    )}Dto } from './dto/update-${sheetName}.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { RequestWithUser } from 'src/auth/interface/requestWithUser';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
      
@Controller('${this.capitalizeFirstLetter(sheetName)}')
@ApiTags('${Comment}')
export class ${this.capitalizeFirstLetter(sheetName)}Controller {
  constructor(private readonly ${sheetName}Service: ${this.capitalizeFirstLetter(
      sheetName,
    )}Service) {}
      
@Post()
@ApiBearerAuth()
@ApiOperation({
  summary: '${Comment}新增接口'
})
create(@Body() create${this.capitalizeFirstLetter(
      sheetName,
    )}Dto: Create${this.capitalizeFirstLetter(sheetName)}Dto,
      @Req() requestwithUser: RequestWithUser,) {
        var user = requestwithUser.user;
  return this.${sheetName}Service.create(create${this.capitalizeFirstLetter(
      sheetName,
    )}Dto, user);
}
      
@Get()
@ApiOperation({
  summary: '${Comment}获取接口'
})
@ApiBearerAuth()
findAll() {
  return this.${sheetName}Service.findAll();
}
      
@Get(':id')
@ApiBearerAuth()
@ApiOperation({
  summary: '${Comment}获取单个数据接口'
})
findOne(@Param('id') id: string) {
  return this.${sheetName}Service.findOne(+id);
}
      
@Patch(':id')
@ApiOperation({
  summary: '${Comment}更新接口'
})
update(@Param('id') id: string, @Body() update${this.capitalizeFirstLetter(
      sheetName,
    )}Dto: Update${this.capitalizeFirstLetter(sheetName)}Dto,
      @Req() requestwithUser: RequestWithUser,) {
        var user = requestwithUser.user;
  return this.${sheetName}Service.update(+id, update${this.capitalizeFirstLetter(
      sheetName,
    )}Dto, user);
}
      
@Delete(':id')
@ApiOperation({
  summary: '${Comment}删除接口'
})
remove(@Param('id') id: string,@Req() requestwithUser: RequestWithUser,) {
  var user = requestwithUser.user;
  return this.${sheetName}Service.remove(+id, user);
  }
}`;
  }

  /**
   * 创建module
   * @param sheetName
   * @returns
   */
  createModule(sheetName): string {
    //构建module
    return `import { Module } from '@nestjs/common';
import { ${this.capitalizeFirstLetter(
      sheetName,
    )}Service } from './${sheetName}.service';
import { ${this.capitalizeFirstLetter(
      sheetName,
    )}Controller } from './${sheetName}.controller';
      
@Module({
  controllers: [${this.capitalizeFirstLetter(sheetName)}Controller],
  providers: [${this.capitalizeFirstLetter(sheetName)}Service]
})
export class ${this.capitalizeFirstLetter(sheetName)}Module {}`;
  }

  /**
   * 创建服务Servicespec
   * @param sheetName
   * @returns
   */
  createServicespec(sheetName): string {
    return `import { Test, TestingModule } from '@nestjs/testing';
    import { ${this.capitalizeFirstLetter(
      sheetName,
    )}Service } from './${sheetName}.service';
          
      describe('${this.capitalizeFirstLetter(sheetName)}Service', () => {
        let service: ${this.capitalizeFirstLetter(sheetName)}Service;
          
        beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
        providers: [${this.capitalizeFirstLetter(sheetName)}Service],
      }).compile();
          
        service = module.get<${this.capitalizeFirstLetter(
          sheetName,
        )}Service>(${this.capitalizeFirstLetter(sheetName)}Service);
      });
          
      it('should be defined', () => {
        expect(service).toBeDefined();
      });
    });
    `;
  }

  /**
   * 构建服务
   * @param sheetName
   * @param primaryKeys
   * @returns
   */
  createService(sheetName: string, primaryKeys, entityName): string {
    //构建服务
    return `import { Injectable, Logger } from '@nestjs/common';
    import { Create${entityName}Dto } from './dto/create-${sheetName}.dto';
    import { Update${entityName}Dto } from './dto/update-${sheetName}.dto';
    import { InjectEntityManager } from '@nestjs/typeorm';
    import { EntityManager } from 'typeorm';
    import { ${entityName} } from './entities/${sheetName}.entity';
          
          @Injectable()
          export class ${entityName}Service {
            private readonly logger = new Logger(${entityName}Service.name);
            constructor(
              @InjectEntityManager()
              private manager: EntityManager,
            ) {}
            async create(create${entityName}Dto: Create${entityName}Dto, user) {
              try {
                const data = await this.manager.save(${entityName}, create${entityName}Dto);
          
                return {
                  controls: {
                    title: '',
                    handleid: data.${primaryKeys[0]},
                    content: create${entityName}Dto,
                    modify_user: user.username,
                    change_at: new Date(),
                  },
                  data: {
                    result: create${entityName}Dto,
                  },
                  msg: '新增数据请求完成',
                };
              } catch (error) {
                throw error;
              }
            }
          
            async findAll() {
              var datas = await this.manager.find(${entityName});
              return {
                data: {
                  result: datas,
                },
                msg: '请求全部数据完成',
              };
            }
          
            async findOne(id: number) {
              try {
                var data = await this.manager.findOne(${entityName}, {
                  where: {
                    ${primaryKeys[0]}: id,
                  },
                });
                return {
                  data: {
                    result: data,
                  },
                  msg: '获取数据请求完成',
                };
              } catch (error) {
                throw error;
              }
            }
          
            async update(id: number, update${entityName}Dto: Update${entityName}Dto, user) {
              try {
                const data = await this.manager.findOne(${entityName}, {
                  where: {
                    ${primaryKeys[0]}: id,
                  },
                });
                Object.assign(data, update${entityName}Dto);
                await this.manager.save(${entityName},data);
                return {
                  controls: {
                    title: '',
                    handleid: data.${primaryKeys[0]},
                    content: data,
                    modify_user: user.username,
                    change_at: new Date(),
                  },
                  data: {
                    result: update${entityName}Dto,
                  },
                  msg: '更新数据请求完成',
                };
              } catch (error) {
                throw error;
              }
            }
    
            async remove(id: number, user) {
              try {
                var data = await this.manager.findOne(${entityName}, {
                  where: {
                    ${primaryKeys[0]}: id,
                  },
                });
                await this.manager.delete(${entityName}, id);
                return {
                  controls: {
                    title: '',
                    handleid: id,
                    content: data,
                    modify_user: user.username,
                    change_at: new Date(),
                  },
                  data: {
                    result: id,
                  },
                  msg: '删除数据请求完成',
                };
              } catch (error) {
                throw error;
              }
            }
          }`;
  }

  exportdoc(file: Express.Multer.File) {
    try {
      const workbook = XLSX.read(file.buffer);
      const sheetNames = workbook.SheetNames;
      sheetNames.forEach(async (sheetTempName) => {
        const sheetName = sheetTempName.split('.')[0];
        const Comment = sheetTempName.split('.')[1];
        const worksheet = workbook.Sheets[sheetTempName];
        const range = XLSX.utils.decode_range(worksheet['!ref']);
        const fields = [];
        const primaryKeys = [];
        const returndata = new Map();
        for (let C = range.s.c; C <= range.e.c; ++C) {
          //字段名
          const filedName =
            worksheet[XLSX.utils.encode_cell({ r: 0, c: C })]?.v.toString();
          //字段类型
          const filedType =
            worksheet[XLSX.utils.encode_cell({ r: 1, c: C })]?.v.toString();
          //字段描述
          const filedComment =
            worksheet[XLSX.utils.encode_cell({ r: 2, c: C })]?.v.toString();

          returndata.set(filedName, '');
        }
        const jsonObject = {};
        returndata.forEach((value, key) => {
          jsonObject[key] = value;
        });
        const jsonstring = JSON.stringify(jsonObject);
        this.Logger.debug(jsonstring);
        this.saveDocTemplateToFile(jsonstring, sheetName);
      });
      return '';
    } catch (error) {
      this.Logger.error(error);
    }
  }

  /**
   * 用于将Controllerspec保存到文件
   * @param entityTemplate
   * @param sheetName
   */
  saveDocTemplateToFile(entityTemplate, sheetName) {
    const DirPath = `./entity_templates/Doc/`;
    if (!fs.existsSync(DirPath)) {
      fs.mkdirSync(DirPath, {
        recursive: true,
      });
    }
    // 定义文件路径和文件名
    const filePath = `./entity_templates/Doc/${sheetName}.json`;
    // 将实体类模板写入文件
    fs.writeFile(filePath, entityTemplate, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`json已经存储到${filePath}`);
    });
  }

  /**
   * 用于将实体类模板保存到文件
   * @param entityTemplate
   * @param sheetName
   */
  saveEntityTemplateToFile(entityTemplate, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/entities/${sheetName}.entity.ts`;
    // 将实体类模板写入文件
    fs.writeFile(filePath, entityTemplate, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`实体已经存储至${filePath}`);
    });
  }

  /**
   * 存储新建dto文件
   * @param createdto
   * @param sheetName
   */
  saveCreateTemplateToFile(createdto, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/dto/create-${sheetName}.dto.ts`;
    // 将新建dto模板写入文件
    fs.writeFile(filePath, createdto, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`CreateDto已经存储至${filePath}`);
    });
  }

  /**
   * 存储更新dto文件
   * @param createdto
   * @param sheetName
   */
  saveUpdateTemplateToFile(updatedto, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/dto/update-${sheetName}.dto.ts`;
    // 将更新dto模板写入文件
    fs.writeFile(filePath, updatedto, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`UpdateDto已经存储至${filePath}`);
    });
  }

  /**
   * 用于将Controllerspec保存到文件
   * @param entityTemplate
   * @param sheetName
   */
  saveControllerspecTemplateToFile(entityTemplate, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/${sheetName}.controller.spec.ts`;
    // 将实体类模板写入文件
    fs.writeFile(filePath, entityTemplate, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`Controllerspec已经存储至${filePath}`);
    });
  }

  /**
   * 存储controller文件
   * @param createdto
   * @param sheetName
   */
  saveControllerTemplateToFile(Controller, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/${sheetName}.controller.ts`;
    // 将Controller模板写入文件
    fs.writeFile(filePath, Controller, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`Controller已经存储至${filePath}`);
    });
  }

  /**
   * 存储Modele文件
   * @param createdto
   * @param sheetName
   */
  saveModeleTemplateToFile(modele, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/${sheetName}.modele.ts`;
    // 将Controller模板写入文件
    fs.writeFile(filePath, modele, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`Controller已经存储至${filePath}`);
    });
  }

  /**
   * 存储ServiceSpec文件
   * @param createdto
   * @param sheetName
   */
  saveServiceSpecTemplateToFile(ServiceSpec, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/${sheetName}.service.spec.ts`;
    // 将Controller模板写入文件
    fs.writeFile(filePath, ServiceSpec, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`ServiceSpec已经存储至${filePath}`);
    });
  }

  /**
   * 存储Service文件
   * @param createdto
   * @param sheetName
   */
  saveServiceTemplateToFile(Service, sheetName) {
    // 定义文件路径和文件名
    const filePath = `./entity_templates/${sheetName}/${sheetName}.service.ts`;
    // 将Controller模板写入文件
    fs.writeFile(filePath, Service, (err) => {
      if (err) {
        this.Logger.error('出现异常,实体存储出错,错误原因:', err);
        return;
      }
      this.Logger.log(`Service已经存储至${filePath}`);
    });
  }

  capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  /**
   * 包含文件
   * @param havefiles
   * @returns
   */
  haveFileTitle(havefiles): string {
    let returnString = '';
    if (havefiles.length > 0) {
      returnString = `@ApiConsumes('multipart/form-data')
@ApiBody({
  description: 'Excel file to upload',
  type: 'multipart/form-data',
  required: true,
  schema: {
    type: 'object',
    properties: {
      file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
@UseInterceptors(FileInterceptor('file')) // 使用 Multer 中间件处理文件上传'))
      `;
    }
    return returnString;
  }

  /**
   * 是否包含文件
   * @param havefiles
   */
  havefile(havefiles): string {
    let returnString = '';
    if (havefiles.length > 0) {
      returnString = `,file`;
    }
    return returnString;
  }

  /**
   * sql对应类型转换
   * @param fieldType
   * @returns
   */
  private sqlFieldType(fieldType: string): any {
    switch (fieldType) {
      case 'int':
        return 'int';
      case 'float':
        return 'float';
      case 'date':
      case 'Date':
      case 'datetime':
        return 'datetime';
      case 'string':
        return 'tinytext';
      case 'longstring':
        return 'longtext';
      default:
        return 'string';
    }
  }

  /**
   * dto对应类型转换
   * @param fieldType
   * @returns
   */
  private dtoFieldType(fieldType: string): any {
    switch (fieldType) {
      case 'int':
      case 'float':
        return 'number';
      case 'datetime':
      case 'date':
        return 'Date';
      default:
      case 'string':
      case 'longstring':
      case 'tinytext':
      case 'longtext':
        return 'string';
    }
  }

  /**
   * 生成entity对应类型转换
   * @param fieldType
   * @returns
   */
  private entityFields(
    fieldType: string,
    isFieldPrimary,
    filedComment,
    filedName,
    filedType,
  ): any {
    //构建主键和字段头标识
    const decorator = isFieldPrimary
      ? `  @PrimaryGeneratedColumn(({
  comment: ${filedComment},
}))`
      : `  @Column({
  comment: ${filedComment},
})`;
    switch (fieldType) {
      case 'int':
        return `${decorator}
    ${filedName}: ${this.dtoFieldType(filedType)};\n`;
      case 'float':
        return 'number';
      case 'datetime':
        return 'datetime';
      case 'string':
        return 'string';
      case 'tinytext':
        return 'string';
      case 'datetime':
        return 'date';
      default:
        return 'string';
    }
  }
}
