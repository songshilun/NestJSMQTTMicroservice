import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ExcelService } from './excel.service';
import { CreateExcelDto } from './dto/create-excel.dto';
import { UpdateExcelDto } from './dto/update-excel.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiProperty,
  ApiTags,
} from '@nestjs/swagger';
import { SkipTokenAuth } from 'src/auth/guard/skip-token-auth.decorator';

@Controller('Excel')
@ApiTags('数据库自动化处理,tip:不知道是什么意思的千万不要乱调用')
export class ExcelController {
  constructor(private readonly excelService: ExcelService) {}

  @Post('CreateTable')
  @ApiOperation({
    summary: '自动化创建对应模块',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: '将EXCEL文件添加到此,并调用接口',
    type: 'multipart/form-data',
    required: true,
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(FileInterceptor('file')) // 使用 Multer 中间件处理文件上传'))
  @SkipTokenAuth()
  CreateTable(@UploadedFile() file: Express.Multer.File) {
    return this.excelService.CreateTable(file);
  }

  @Post('ExportDoc')
  @UseInterceptors(FileInterceptor('file')) // 使用 Multer 中间件处理文件上传'))
  exportdoc(@UploadedFile() file: Express.Multer.File) {
    return this.excelService.exportdoc(file);
  }
}
