import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { MqttModule } from './service/mqtt/mqtt.module';
import { RoleModule } from './role/role.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [ConfigModule, MqttModule, RoleModule, UserModule],
  controllers: [AppController],
  providers: [AppService, ConfigService],
})
export class AppModule {}
