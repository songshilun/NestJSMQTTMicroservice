import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import * as path from 'path';
import * as fs from 'fs';

const logger = new Logger('Main');

async function bootstrap() {
  const rootPath = process.cwd(); // 当前文件的目录
  const configFilePath = path.join(rootPath, 'config.json'); // 完整的配置文件路径
  // const parentDir = path.dirname(rootPath); // 上一级目录
  // const grandParentDir = path.dirname(parentDir); // 上一级的上一级目录

  // 读取配置文件
  const configData = fs.readFileSync(configFilePath, 'utf8');
  const config = JSON.parse(configData);

  const microServiceOptions = {
    transport: Transport.MQTT,
    options: {
      host: config.MQTT.host,
      port: config.MQTT.port,
    },
  };

  const app = await NestFactory.createMicroservice(
    AppModule,
    microServiceOptions,
  );

  app.listen();
  logger.log(`事件中心微服务已经上线`);
}
bootstrap();
