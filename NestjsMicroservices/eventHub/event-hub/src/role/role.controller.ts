import { Controller } from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  MessagePattern,
  Payload,
  Transport,
} from '@nestjs/microservices';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { MqttService } from 'src/service/mqtt/mqtt.service';
import { ConfigService } from 'src/config/config.service';

@Controller()
export class RoleController {
  constructor(private readonly configService: ConfigService) {}

  private client: ClientProxy;

  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
  }

  @MessagePattern('MessageHub-Role-Create')
  async create(@Payload() createRoleDto: CreateRoleDto) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[角色新增请求],请求的数据是:[角色名称]${createRoleDto.role_name} [关联权限]${createRoleDto.role_permission},正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('Role-Create', createRoleDto);
  }

  @MessagePattern('MessageHub-Role-FindAll')
  async findAll() {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[角色获取列表请求],正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('Role-FindAll', '');
  }

  @MessagePattern('MessageHub-Role-FindOne')
  async findOne(@Payload() id: number) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[角色获取单个数据请求],请求的数据是:[主键ID]:${id},正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('Role-FindOne', id);
  }

  @MessagePattern('MessageHub-Role-Update')
  async update(@Payload() data) {
    const updateRoleDto: UpdateRoleDto = data.updateRoleDto;
    const id: number = data.id;
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[角色更新请求],请求的数据是:[主键ID]:${id},[角色名称]${updateRoleDto.role_name} [关联权限]${updateRoleDto.role_permission},正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('Role-Update', updateRoleDto);
  }

  @MessagePattern('MessageHub-Role-Remove')
  async remove(@Payload() id: number) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[角色删除请求],请求的数据是:[主键ID]:${id},正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('Role-Remove', id);
  }
}
