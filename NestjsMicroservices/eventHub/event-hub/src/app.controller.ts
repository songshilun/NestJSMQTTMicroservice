import { Controller, Get, OnModuleInit } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigService } from './config/config.service';
import {
  ClientProxy,
  ClientProxyFactory,
  MessagePattern,
  Payload,
  Transport,
} from '@nestjs/microservices';
import { LoginOutDto, LoginStateChange, UserLoginDto } from './model/user.dto';
import { MqttService } from './service/mqtt/mqtt.service';

@Controller()
export class AppController implements OnModuleInit {
  constructor(
    private readonly appService: AppService,
    private readonly mqttService: MqttService,
    private readonly configService: ConfigService,
  ) {}

  private client: ClientProxy;

  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
    this.mqttService.emit('Log', '事件中心微服务已上线');
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @MessagePattern('MessageHub-system-GetAllDict')
  async systemGetAllDict(@Payload() type) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[获取字典列表请求],请求数据:${type},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-getAllDict', type);
  }

  @MessagePattern('MessageHub-system-getSystemDictRowByName')
  async getSystemDictRowByName(@Payload() name) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[获取特定字典],请求数据:${name},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-getSystemDictRowByName', name);
  }

  @MessagePattern('MessageHub-system-create')
  async systemCreate(@Payload() data) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[新建字典],请求数据:${data},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-create', data);
  }

  /**
   *
   * @param data
   * @returns
   */
  @MessagePattern('MessageHub-system-createSystemDictRowByName')
  async createSystemDictRowByName(@Payload() data) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[新建字典的列],请求数据:${data},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-createSystemDictRowByName', data);
  }

  /**
   * 通过字典名更新字典
   * @param data
   * @returns
   */
  @MessagePattern('MessageHub-system-updateDictByName')
  async updateDictByName(@Payload() data) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[通过字典名更新字典],请求数据:${data},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-updateDictByName', data);
  }

  /**
   * 通过字典名和字段ID 更新特定字段信息
   * @param data
   * @returns
   */
  @MessagePattern('MessageHub-system-updateDictRowByNameAndRowID')
  async updateDictRowByNameAndRowID(@Payload() data) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[通过字典名更新字典],请求数据:${data},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-updateDictRowByNameAndRowID', data);
  }

  /**
   * 删除特定字典
   * @param data
   * @returns
   */
  @MessagePattern('MessageHub-system-deleteSystemDictRowByName')
  async deleteSystemDictRowByName(@Payload() data) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[通过字典名更新字典],请求数据:${data},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-deleteSystemDictRowByName', data);
  }

  /**
   * 删除特定字典的特定Row
   * @param data
   * @returns
   */
  @MessagePattern('MessageHub-system-deleteDictRowByNameAndRowID')
  async deleteDictRowByNameAndRowID(@Payload() data) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[通过字典名更新字典],请求数据:${data},正在推送至[业务资源微服务]`,
    );
    return this.client.send('system-deleteDictRowByNameAndRowID', data);
  }
}
