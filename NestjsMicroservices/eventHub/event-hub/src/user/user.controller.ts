import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { UserService } from './user.service';
import { CreateUserDto, ResetPassword } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ConfigService } from 'src/config/config.service';
import {
  LoginOutDto,
  LoginStateChange,
  UserLoginDto,
} from 'src/model/user.dto';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @MessagePattern('MessageHub-User-Create')
  async create(@Payload() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @MessagePattern('MessageHub-User-FindAll')
  async findAll() {
    return this.userService.findAll();
  }

  @MessagePattern('MessageHub-User-FindOne')
  async findOne(@Payload() id: number) {
    return this.userService.findOne(id);
  }

  @MessagePattern('MessageHub-User-Update')
  async update(@Payload() data) {
    return this.userService.update(data);
  }

  @MessagePattern('MessageHub-User-Remove')
  async remove(@Payload() id: number) {
    return this.userService.remove(id);
  }

  @MessagePattern('MessageHub-User-login')
  async userslogin(@Payload() data) {
    return this.userService.login(data);
  }

  @MessagePattern('MessageHub-User-logOut')
  async usersloginOut(@Payload() loginOutDto: LoginOutDto) {
    return this.userService.usersloginOut(loginOutDto);
  }

  @MessagePattern('MessageHub-User-ResetPassword')
  async ResetPassword(@Payload() ResetPassword: ResetPassword) {
    return this.userService.ResetPassword(ResetPassword);
  }
}
