import { Injectable, Logger } from '@nestjs/common';
import { CreateUserDto, ResetPassword } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { ConfigService } from 'src/config/config.service';
import {
  LoginOutDto,
  LoginStateChange,
  UserLoginDto,
} from 'src/model/user.dto';

@Injectable()
export class UserService {
  constructor(private readonly configService: ConfigService) {}
  private readonly logger = new Logger(UserService.name);
  private client: ClientProxy;

  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
  }

  async create(createUserDto: CreateUserDto) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户管理新建请求],请求的数据是:[账号]${createUserDto.account},[手机号]${createUserDto.phone_number},[姓名]${createUserDto.name},[部门]${createUserDto.department},[角色]${createUserDto.role},[默认密码]${createUserDto.default_password},[账号权限]${createUserDto.permissions},正在推送至[用户与鉴权微服务]`,
    );
    return await this.client.send<CreateUserDto>('User-Create', createUserDto);
  }

  async findAll() {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户管理获取列表请求],正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('User-FindAll', '');
  }

  async findOne(id: number) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户管理获取单个数据请求],请求的数据是:[主键ID]:${id},正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('User-FindOne', id);
  }

  async update(data) {
    const updateUserDto: UpdateUserDto = data.updateUserDto;
    const id: number = data.id;
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户管理更新请求],请求的数据是:[主键ID]:${id},[数据]:${JSON.stringify(
        updateUserDto,
      )},正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('User-Update', data);
  }

  async remove(id: number) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户管理删除请求],请求的数据是:[主键ID]:${id},正在推送至[用户与鉴权微服务]`,
    );
    return this.client.send('User-Remove', id);
  }

  async login(data) {
    this.logger.log(data);
    const userLoginDto: UserLoginDto = data.UserLoginDto;
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户登录请求],登录的信息是:[用户名]${userLoginDto.username} [密码]${userLoginDto.password},正在推送至[鉴权微服务]`,
    );
    return this.client.send('User-Login', data);
  }

  async usersloginOut(loginOutDto: LoginOutDto) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户登录下线请求],下线的用户是${loginOutDto.user_id},正在推送至[鉴权微服务]`,
    );
    //构建登录状态变更结构体并推送到队列内
    const loginstatechange = new LoginStateChange();
    loginstatechange.userid = loginOutDto.user_id;
    loginstatechange.state = false;
    return this.client.send('User-LoginStateChange', loginOutDto);
  }

  async ResetPassword(ResetPassword: ResetPassword) {
    await this.client.emit<string>(
      'Log',
      `[事件中心微服务]接收到[用户重置密码请求],要重置密码的用户是${
        ResetPassword.user_id
      },[数据]:${JSON.stringify(ResetPassword)},正在推送至[鉴权微服务]`,
    );
    return this.client.send('User-ResetPassword', ResetPassword);
  }
}
