export class CreateUserDto {
  name: string;

  account: string;

  password: string;

  role: number;

  ass_user_role_id: number;

  user_state: number;

  login_state: number;

  phone_number: string;

  department: string;

  default_password: string;

  permissions: string;

  add_user_id: number;

  add_user: string;

  create_at: Date;

  update_at: Date;
}

export class ResetPassword {
  /**
   * 用户id
   */
  user_id: number;

  /**
   * 旧密码
   */
  old_password: string;

  /**
   * 新密码
   */
  new_password: string;
}
