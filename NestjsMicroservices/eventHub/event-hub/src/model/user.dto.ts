export class CreateUserDto {}

export class UserLoginDto {
  username: string;

  password: string;
}

export class LoginOutDto {
  user_id: number;
}

export class LoginStateChange {
  /**
   * 用户id
   */
  userid: number;

  /**
   * 登录状态变更
   */
  state: boolean;
}
