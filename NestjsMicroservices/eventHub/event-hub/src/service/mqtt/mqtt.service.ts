import { Injectable, OnModuleInit, OnModuleDestroy } from '@nestjs/common';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class MqttService implements OnModuleInit, OnModuleDestroy {
  private client: ClientProxy;

  constructor(private readonly configService: ConfigService) {}

  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
  }

  onModuleDestroy() {
    this.client.close();
  }

  /**
   * MQTT推送消息不等待消息被消息返回
   */
  emit(topic: string, message: string | Buffer) {
    this.client.emit(topic, message);
  }

  /**
   * MQTT推送消息并等待消息被消费后返回
   * @param topic
   * @param message
   */
  async send(topic: string, message: string | Buffer | object) {
    this.client.send(topic, message);
  }
}
