module.exports = {
  apps: [
    {
      name: "项目管理-业务-日志微服务",
      script: "businesslogic/log-microservice/dist/main.js",
      instances: "1", // 根据需求设置实例数量
      exec_mode: "fork",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "production",
      },
    },
    {
      name: "项目管理-通信-HTTP微服务",
      script: "communication/http-server/dist/main.js",
      instances: "1", // 根据需求设置实例数量
      exec_mode: "fork",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "production",
      },
    },
    {
      name: "项目管理-通信-SocketIO微服务",
      script: "communication/socketio-server/dist/main.js",
      instances: "1", // 根据需求设置实例数量
      exec_mode: "fork",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "production",
      },
    },
    {
      name: "项目管理-中间件-事件中心微服务",
      script: "eventHub/event-hub/dist/main.js",
      instances: "1", // 根据需求设置实例数量
      exec_mode: "fork",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "production",
      },
    },
    {
      name: "项目管理-业务-鉴权用户微服务",
      script: "businesslogic/users-microservice/dist/main.js",
      instances: "1", // 根据需求设置实例数量
      exec_mode: "fork",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "production",
      },
    },
    {
      name: "项目管理-业务-业务资源微服务",
      script: "businesslogic/asset-microservice/dist/main.js",
      instances: "1", // 根据需求设置实例数量
      exec_mode: "fork",
      autorestart: true,
      watch: false,
      max_memory_restart: "1G",
      env: {
        NODE_ENV: "production",
      },
    },
  ],
};
