import { Controller, Get, Logger, OnModuleInit } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController implements OnModuleInit {
  constructor(private readonly appService: AppService) {}
  private readonly logger = new Logger('日志微服务');
  onModuleInit() {
    this.logger.log(`日志微服务已经上线`);
  }

  async getHello() {}
}
