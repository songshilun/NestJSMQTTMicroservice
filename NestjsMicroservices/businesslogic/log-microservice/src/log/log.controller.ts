import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { LogService } from './log.service';
import { CreateLogDto } from './dto/create-log.dto';
import { UpdateLogDto } from './dto/update-log.dto';

@Controller()
export class LogController {
  constructor(private readonly logService: LogService) {}

  @MessagePattern('Log')
  log(@Payload() msg: string) {
    return this.logService.log(msg);
  }

  @MessagePattern('LogError')
  logError(@Payload() errormsg: string) {
    return this.logService.logError(errormsg);
  }
}
