import { Injectable, Logger } from '@nestjs/common';
import { CreateLogDto } from './dto/create-log.dto';
import { UpdateLogDto } from './dto/update-log.dto';

@Injectable()
export class LogService {
  private readonly logger = new Logger('日志微服务');

  log(msg: string) {
    this.logger.log(msg);
    return '';
  }

  logError(errormsg: string) {
    this.logger.error(errormsg);
    return '';
  }
}
