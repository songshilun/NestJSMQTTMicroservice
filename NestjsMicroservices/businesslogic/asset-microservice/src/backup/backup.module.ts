import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { backService } from './backup.service';
import { ConfigModule } from 'src/config/config.module';

@Module({
  imports: [ScheduleModule.forRoot(),ConfigModule],
  providers: [backService],
})
export class BackupModule {}