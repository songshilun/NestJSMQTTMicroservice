import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { exec } from 'child_process';
import { ConfigService } from '../config/config.service';
import * as mysqldump from 'mysqldump';
import * as moment from 'moment';

@Injectable()
export class backService {
  private readonly logger = new Logger(backService.name);

  constructor(private readonly configService: ConfigService) {}

  @Cron('0 8 * * *') // 使用 Cron 表达式定义定时任务的执行时间
  async handleCron() {
    const mysqldump = require('mysqldump');
    // 定义数据库连接信息
    const dbhost = this.configService.getDatabaseConfig().host;
    const dbport = this.configService.getDatabaseConfig().port;
    const dbUsername = this.configService.getDatabaseConfig().username;
    const dbPassword = this.configService.getDatabaseConfig().password;
    const dbName = this.configService.getDatabaseConfig().databaseName;
    const formattedDateTime = moment().format('YYYYMMDD_HHmm');
    // 定义备份文件路径
    const backupFilePath = `./backups/database-${formattedDateTime}.sql`;

    // MySQL 连接配置
    const dbConfig = {
      connection: {
        host: dbhost,
        port: dbport,
        user: dbUsername,
        password: dbPassword,
        database: dbName,
      },
      dumpToFile: `${backupFilePath}`,
    };
    // 执行备份
    mysqldump(dbConfig)
      .then(() => {
        this.logger.debug(
          `数据库备份于${formattedDateTime}执行完成,备份在服务器目录backups`,
        );
      })
      .catch((err) => {
        this.logger.error('数据库备份执行失败,请开发人员关注,错误信息是:', err);
      });
  }
}
