import { Controller } from '@nestjs/common';
import { SystemService } from './system.service';
import { Logger } from '@nestjs/common';
import { TokenAuthGuard } from 'src/auth/guard/token.authguard';
import { SkipTokenAuth } from 'src/auth/guard/skip-token-auth.decorator';
import { ApiOperation } from '@nestjs/swagger';
import { MessagePattern, Payload } from '@nestjs/microservices';

@Controller()
export class SystemController {
  private readonly logger = new Logger(SystemController.name);
  constructor(private readonly systemService: SystemService) {}

  /**
   * 获取字典列表
   * @param type 获取字典类型 默认为system
   * @returns
   */
  @MessagePattern('system-getAllDict')
  getAllDict(@Payload() type: string) {
    // 类型判断
    switch (type) {
      case 'business':
        break;
      case 'all':
        type = '*';
        break;
      default:
        type = 'system';
        break;
    }
    return this.systemService.getAllDict(type);
  }

  /**
   * 获取特定系统字典
   * @param type 获取字典类型 默认为system
   * @returns
   */
  @MessagePattern('system-getSystemDictRowByName')
  async getSystemDictRowByName(@Payload() name: string) {
    if (!name) {
      return {
        data: [],
        msg: 'name必须传入',
        code: -1,
      };
    }

    const dicname = 'system_dict_' + name;
    this.logger.log('获取特定系统字典' + dicname);

    let result = await this.systemService.getSystemDictRowByName(dicname);
    let dict = [];
    let maxDepth = 0;
    if (Array.isArray(result)) {
      // 如果字典为空
      if (result.length === 0) {
        maxDepth = 0;
        dict = [];
      } else {
        // 确定最大深度
        await (result as any[]).forEach((element) => {
          if (element.depth > maxDepth) maxDepth = element.depth;
        });

        let temp = new Map();
        temp.set('root', []);
        for (let i = maxDepth; i > 0; i--) {
          // 遍历循环每一字段
          for (let index = 0; index < result.length; index++) {
            const element = result[index];
            if (element.depth !== i) continue;
            // 添加字段到缓存
            if (element.depth !== 1) {
              if (!temp.has(element.parent_id)) {
                temp.set(element.parent_id, []);
              }
              temp.get(element.parent_id).push(element);
              // result.splice(index, 1);
            } else {
              temp.get('root').push(element);
            }

            if (temp.has(element.id)) {
              element.hasChildren = true;
              element.children = temp.get(element.id);
              temp.delete(element.id);
            }
          }
        }
        for (const i of temp.values()) {
          dict.push(i);
        }
        dict = dict[0];
      }
    } else {
      dict = result;
    }
    return {
      data: {
        result: dict,
      },
      msg: '获取字典成功',
      code: 1,
    };
  }

  /**
   * 新建字典
   * @param name 字典英文名
   * @param title 字典中文名
   * @param type 是否封存 默认0不封存 [0 | 1]
   * @param sealed 备注
   * @param comment 字典业务类型 默认system  [ system | business]
   * @returns
   */
  @MessagePattern('system-create')
  create(@Payload() data) {
    let name: string = data.name;
    let title: string = data.title;
    let type: string = data.type;
    let sealed: number = data.sealed;
    let comment: string = data.comment;
    // 业务判空
    if (
      name === '' ||
      name === undefined ||
      title === '' ||
      title === undefined
    )
      throw '缺少参数';
    // 类型判断
    switch (type) {
      case 'business':
        break;
      default:
        type = 'system';
        break;
    }
    //新建字典时不需要组合system_dict_前缀,sql语句中已经包含
    //此处的name为字典英文名,不是数据库表名,数据库表名为system_dict_name
    return this.systemService.create(name, title, type, sealed, comment);
  }

  /**
   * 创建字典的ROW
   * @param {*} name 名称
   * @param {*} enumValue 枚举值
   * @param {*} code 代码
   * @param {*} parent_id 父节点ID
   * @param {*} depth 树结构深度
   * @param {*} sorting_id 排序代码
   * @param {*} sealed 封存
   * @param {*} comment 注释
   * @returns
   */
  @MessagePattern('system-createSystemDictRowByName')
  createSystemDictRowByName(@Payload() data) {
    let name: string = data.name;
    let parent_id: number = data.parent_id;
    let depth: number = data.depth;
    let enumTitle: string = data.enumTitle;
    let enumValue: string = data.enumValue;
    let code: string = data.code;
    let sorting_id: number = data.sorting_id;
    let sealed: number = data.sealed;
    let comment: string = data.comment;
    if (!name) {
      return {
        data: [],
        msg: 'name必须传入',
        code: -1,
      };
    }
    const dicname = 'system_dict_' + name;
    return this.systemService.createSystemDictRowByName(
      dicname,
      enumTitle,
      enumValue,
      code,
      parent_id,
      depth,
      sorting_id,
      sealed,
      comment,
    );
  }

  /**
   * 通过字典名和字段ID 更新特定字段信息
   * @param {*} name
   * @param {*} rowID
   * @param {*} enumValue
   * @param {*} code
   * @param {*} sorting_id
   * @param {*} sealed
   * @returns
   */
  @MessagePattern('system-updateDictRowByNameAndRowID')
  updateDictRowByNameAndRowID(@Payload() data) {
    let name: string = data.name;
    let rowID: number = data.rowID;
    let enumTitle: string = data.enumTitle;
    let enumValue: string = data.enumValue;
    let code: string = data.code;
    let sorting_id: number = data.sorting_id;
    let sealed: number = data.sealed;
    let comment: string = data.comment;

    if (!name) throw 'Name必须传入';
    const dicname = 'system_dict_' + name;
    if (!comment) comment = '';
    return this.systemService.updateDictRowByNameAndRowID(
      dicname,
      rowID,
      enumTitle,
      enumValue,
      code,
      sorting_id,
      sealed,
      comment,
    );
  }

  /**
   * 通过字典名更新字典
   * @param {*} name 字典英文名
   * @param {*} title 字典中文名
   * @param {*} comment 注释
   * @param {*} sealed 封存
   * @returns
   */
  @MessagePattern('system-updateDictByName')
  updateDictByName(@Payload() data) {
    const name: string = data.name;
    const title: string = data.title;
    const comment: string = data.comment;
    const sealed: number = data.sealed;
    if (!name) throw 'Name必须传入';
    const dicname = 'system_dict_' + name;
    return this.systemService.updateDictByName(dicname, title, comment, sealed);
  }

  /**
   * 删除特定字典
   * @param name 字典名
   */
  @MessagePattern('system-deleteSystemDictRowByName')
  deleteSystemDictRowByName(@Payload() name: string) {
    if (!name) throw 'Name必须传入';
    const dicname = 'system_dict_' + name;
    return this.systemService.deleteSystemDictRowByName(dicname);
  }

  /**
   * 删除特定字典的特定Row
   * @param name 系统字典的名称
   * @param rowID 上述系统字典的中的RowID
   */
  @MessagePattern('system-deleteDictRowByNameAndRowID')
  deleteDictRowByNameAndRowID(@Payload() data) {
    const name: string = data.name;
    const rowID: number = data.rowID;
    if (!name) throw 'Name必须传入';
    const dicname = 'system_dict_' + name;
    return this.systemService.deleteDictRowByNameAndRowID(dicname, rowID);
  }
}
