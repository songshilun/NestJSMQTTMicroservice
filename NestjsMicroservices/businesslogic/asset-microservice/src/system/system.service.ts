/*
 * @作者: 宋时轮
 * @Date: 2023-03-13 10:01:11
 * @LastEditors: 宋时轮
 * @LastEditTime: 2023-07-07 10:01:11
 * @=: ============================================================
 * @版本: v1.0
 * @描述: 字典服务
 */
import { Injectable, OnModuleInit } from '@nestjs/common';
import { CreateSystemDto } from './dto/create-system.dto';
import { UpdateSystemDto } from './dto/update-system.dto';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { Logger } from '@nestjs/common';
import { error } from 'console';
import {
  Behaviour_static,
  Comprehensive_support_static,
  Equip_type_static,
  Formation_static,
  Weapon_type_static,
} from './entities/systables.entity';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class SystemService implements OnModuleInit {
  private readonly logger = new Logger(SystemService.name);
  constructor(
    @InjectEntityManager()
    private manager: EntityManager,
    private readonly configService: ConfigService,
  ) {}
  private client: ClientProxy;
  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
    this.logger.log('登录服务消息队列通讯初始化完成...');
  }

  /**
   * 获取字典列表
   * @param type 获取字典类型 默认为system
   * @returns
   */
  async getAllDict(type) {
    await this.client.emit<string>(
      'Log',
      `[资源微服务]接收到[字典请求],请求的信息是:[类型]:${type},正在处理请求.....`,
    );
    let sql = `SELECT * FROM SYS_TABLES WHERE type = '${type}' AND category = 'system_dict';`;
    if (type === '*')
      sql = `SELECT * FROM SYS_TABLES WHERE category = 'system_dict';`;
    try {
      let result = await this.manager.query(sql);
      let fields = [];
      result.forEach((e) => {
        const { id, name, title, sealed, comment } = e;
        fields.push({
          id,
          name: name.slice(12), // 剔除附加字符 "system_dict_"
          title,
          sealed: Boolean(sealed[0]),
          comment,
        });
      });

      return {
        data: {
          result: fields,
        },
        msg: '获取字典成功',
        code: 1,
      };
    } catch (e) {
      this.logger.error(e);
      throw new Error(e.sqlMessage);
    }
  }

  /**
   * 新建字典
   * @param name 字典英文名
   * @param title 字典中文名
   * @param type 是否封存 默认0不封存 [0 | 1]
   * @param sealed 备注
   * @param comment 字典业务类型 默认system  [ system | business]
   * @returns
   */
  async create(name, title, type, sealed, comment) {
    sealed = sealed >= 0 ? (sealed > 0 ? 1 : 0) : 0;

    const createTableSql = `
        CREATE TABLE \`system_dict_${name}\` (
            id INT UNSIGNED NOT NULL AUTO_INCREMENT,
            enumTitle VARCHAR(255) NOT NULL,
            enumValue VARCHAR(255) NOT NULL,
            code VARCHAR(255) NOT NULL,
            sorting_id INT UNSIGNED NOT NULL,
            parent_id INT UNSIGNED NOT NULL DEFAULT 0,
            depth INT UNSIGNED NOT NULL DEFAULT 1,
            sealed bit NOT NULL DEFAULT 0,
            comment TEXT NULL COMMENT '注释',
            create_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
            modify_time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            UNIQUE (enumValue),
            UNIQUE (code),
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
    `;
    //UNIQUE (sorting_id),

    const insertTableSql = `
        INSERT INTO SYS_TABLES(name, title, type, sealed, category, comment)
        VALUES('system_dict_${name}', '${title}', '${type}', ${sealed}, 'system_dict', '${
      comment ?? ''
    }');
    `;

    try {
      await this.manager.query(createTableSql);
      await this.manager.query(insertTableSql);
      return {
        msg: '新建字典成功',
        code: 1,
      };
    } catch (e) {
      throw new Error('新建字典错误');
    }
  }

  /**
   * 获取特定系统字典
   * @returns
   */
  async getSystemDictRowByName(name) {
    try {
      // var queryResult = await this.manager.query(
      //   `select *
      //   from information_schema.TABLES
      //   where TABLE_NAME = "${name}";`
      // );
      // this.logger.log(queryResult);
      // if(queryResult.length <= 0){
      //   throw `没有找到ID:<${name}>对应的数据表`;
      // }
      // this.logger.log(queryResult);
      //查询数据库表中的所有数据
      var result1 = await this.manager.query(
        `SELECT * FROM ${name} WHERE 1 = 1;`,
      );
      await result1.forEach((e) => {
        // 根据具体情况转换为布尔值
        // 转换为布尔-值
        e.sealed = Boolean(e.sealed[0]);
      });
      return result1;
    } catch (e) {
      return {
        data: [],
        msg: `${e}`,
        code: -1,
      };
    }
  }

  /**
   * 创建字典的ROW
   * @param {*} name 名称
   * @param {*} enumTitle 枚举标题
   * @param {*} enumValue 枚举值
   * @param {*} code 代码
   * @param {*} parent_id 父节点ID
   * @param {*} depth 树结构深度
   * @param {*} sorting_id 排序代码
   * @param {*} sealed 封存
   * @param {*} comment 注释
   * @returns
   */
  async createSystemDictRowByName(
    name,
    enumTitle,
    enumValue,
    code,
    parent_id,
    depth,
    sorting_id,
    sealed,
    comment,
  ) {
    if (sealed >= 0) sealed = sealed > 0 ? 1 : 0;
    if (!sealed) sealed = 0;
    try {
      let result = await this.manager.query(
        `INSERT INTO ${name}(enumTitle,enumValue,code,parent_id,depth,sorting_id,sealed,comment) VALUES('${enumTitle}','${enumValue}','${code}',${+parent_id},${+depth},${+sorting_id},${+sealed},'${
          comment ?? ''
        }');`,
      );

      return {
        data: {
          result: result,
        },
        msg: '新建一条字典数据成功',
        code: 1,
      };
    } catch (e) {
      throw new Error(e.sqlMessage);
    }
  }

  /**
   * 通过字典名更新字典
   * @param {*} name 字典英文名
   * @param {*} title 字典中文名
   * @param {*} comment 注释
   * @param {*} sealed 封存
   * @returns
   */
  async updateDictByName(name, title, comment, sealed) {
    if (
      (name === null || name === '') &&
      (title === null || title === '') &&
      (comment === null || comment === '') &&
      (sealed === null || sealed === '')
    )
      throw new Error('缺少参数');

    // 填充参数数组
    let fields = { title, comment, sealed };
    if (title) fields.title = title;
    if (comment) fields.comment = comment;
    if (sealed >= 0) fields.sealed = sealed > 0 ? 1 : 0;

    let innerSQL = '';
    let i = 0;
    for (const key in fields) {
      if (i > 0) innerSQL += ',';
      if (key === 'sealed') {
        innerSQL += `${key}=${fields[key]}`;
      } else {
        innerSQL += `${key}='${fields[key]}'`;
      }
      i++;
    }

    try {
      let result = await this.manager.query(
        `UPDATE SYS_TABLES SET ${innerSQL} WHERE name = '${name}' AND category = 'system_dict';`,
      );
      return {
        data: {
          result: result,
        },
        msg: '更新字典成功',
        code: 1,
      };
    } catch (e) {
      throw new Error(e.sqlMessage);
    }
  }

  /**
   * 通过字典名和字段ID 更新特定字段信息
   * @param {*} name
   * @param {*} rowID
   * @param {*} enumValue
   * @param {*} code
   * @param {*} sorting_id
   * @param {*} sealed
   * @returns
   */
  async updateDictRowByNameAndRowID(
    name,
    rowID,
    enumTitle,
    enumValue,
    code,
    sorting_id,
    sealed,
    comment,
  ) {
    if ((name === null || name === '') && (rowID === null || rowID === ''))
      throw new Error('缺少参数');

    // 填充参数数组
    let fields = { enumTitle, enumValue, code, sorting_id, sealed, comment };
    if (enumValue) fields.enumValue = enumValue;
    if (enumTitle) fields.enumTitle = enumTitle;
    if (code) fields.code = code;
    if (sorting_id >= 0) fields.sorting_id = sorting_id;
    if (sealed >= 0) fields.sealed = sealed > 0 ? 1 : 0;
    if (comment) fields.comment = comment;

    let innerSQL = '';
    let i = 0;
    for (const key in fields) {
      if (i > 0) innerSQL += ',';
      if (key === 'sealed' || key === 'sorting_id') {
        innerSQL += `${key}=${fields[key]}`;
      } else {
        innerSQL += `${key}='${fields[key]}'`;
      }
      i++;
    }

    try {
      let result = await this.manager.query(
        `UPDATE \`${name}\` SET ${innerSQL} WHERE id = ${rowID};`,
      );
      return {
        data: {
          result: result,
        },
        msg: '更新字典成功',
        code: 1,
      };
    } catch (e) {
      throw new Error(e.sqlMessage);
    }
  }

  /**
   * 删除特定字典
   * @param name 字典名
   */
  async deleteSystemDictRowByName(name) {
    try {
      let Select = `SELECT * from SYS_TABLES WHERE name = '${name}';`;
      const [rows] = await this.manager.query(Select);
      if (rows.length === 0) throw `没有找到ID:<${name}>对应的数据表`;
      let DeleteSql = `DELETE from SYS_TABLES WHERE name = '${name}';`;
      let DropSql = `DROP TABLE \`${name}\`;`;
      await this.manager.query(DeleteSql);
      let result = await this.manager.query(DropSql);

      return {
        data: {
          result: result,
        },
        msg: '删除字典成功',
        code: 1,
      };
    } catch (e) {
      throw new Error(e.sqlMessage);
    }
  }

  /**
   * 删除特定字典的特定Row
   * @param name 系统字典的名称
   * @param rowID 上述系统字典的中的RowID
   */
  async deleteDictRowByNameAndRowID(name, rowID) {
    try {
      const TemprowID = rowID;
      let result = await this.manager.query(
        `SELECT * FROM ${name} WHERE id = ${TemprowID};`,
      );
      this.logger.log(result[0], TemprowID);

      await this.deleteChildren(name, TemprowID);

      if (result[0].length === 0) {
        throw new Error('没有找到相应的Row');
      }
      let DeleteSql = `DELETE FROM ${name} WHERE id = ${TemprowID};`;
      await this.manager.query(DeleteSql);

      return {
        data: {
          result: DeleteSql,
        },
        msg: '删除特定字典的特定Row成功',
        code: 1,
      };
    } catch (e) {
      throw new Error(e.sqlMessage);
    }
  }
  /**
   * 递归删除子节点
   * @param name  表名
   * @param parentId 父级ID
   */
  private async deleteChildren(name: string, parentId: number): Promise<void> {
    //查询数据库中parent_id为rowID的数据
    let childEntities = await this.manager.query(
      `SELECT * FROM ${name} WHERE parent_id = ${parentId};`,
    );
    for (const child of childEntities) {
      await this.deleteChildren(name, child.id); // Recursive call to delete child's children
      let childEntities = await this.manager.query(
        `DELETE FROM ${name} WHERE id = ${child.id};`,
      );
    }
  }

  async nonAuthGetEquip_type_static() {
    var datas = await this.manager.query(
      'SELECT * FROM system_dict_equip_type_static WHERE 1 = 1;',
    );

    const returnData = [];
    for (const data of datas) {
      const tempdata = new Equip_type_static();
      tempdata.equip_type_static_id = data.enumValue;
      tempdata.equip_type_static_name = data.enumTitle;
      returnData.push(tempdata);
    }
    return {
      data: {
        result: returnData,
      },
      msg: '请求全部数据完成',
    };
  }

  async nonAuthGetWeaponTypeStaticDic() {
    var datas = await this.manager.query(
      'SELECT * FROM system_dict_weapon_type_static WHERE 1 = 1;',
    );

    const returnData = [];
    for (const data of datas) {
      const tempdata = new Weapon_type_static();
      tempdata.weapon_type_static_id = data.enumValue;
      tempdata.weapon_type_static_name = data.enumTitle;
      returnData.push(tempdata);
    }
    return {
      data: {
        result: returnData,
      },
      msg: '请求全部数据完成',
    };
  }

  async nonAuthGetComprehensiveSupportDic() {
    var datas = await this.manager.query(
      'SELECT * FROM system_dict_Comprehensive_support_type WHERE 1 = 1;',
    );

    const returnData = [];
    for (const data of datas) {
      const tempdata = new Comprehensive_support_static();
      tempdata.Comprehensive_support_id = data.enumValue;
      tempdata.Comprehensive_support_name = data.enumTitle;
      returnData.push(tempdata);
    }
    return {
      data: {
        result: returnData,
      },
      msg: '请求全部数据完成',
    };
  }

  async nonAuthGetFormationDic() {
    var datas = await this.manager.query(
      'SELECT * FROM system_dict_formation_type WHERE 1 = 1;',
    );

    const returnData = [];
    for (const data of datas) {
      const tempdata = new Formation_static();
      tempdata.Formation_id = data.enumValue;
      tempdata.Formation_name = data.enumTitle;
      returnData.push(tempdata);
    }
    return {
      data: {
        result: returnData,
      },
      msg: '请求全部数据完成',
    };
  }

  async nonAuthGetBehaviourDic() {
    var datas = await this.manager.query(
      'SELECT * FROM system_dict_behavior_type WHERE 1 = 1;',
    );

    const returnData = [];
    for (const data of datas) {
      const tempdata = new Behaviour_static();
      tempdata.Behaviour_id = data.enumValue;
      tempdata.Behaviour_name = data.enumTitle;
      returnData.push(tempdata);
    }
    return {
      data: {
        result: returnData,
      },
      msg: '请求全部数据完成',
    };
  }
}
