import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { AuthModule } from './auth/auth.module';
import { JwtModule } from '@nestjs/jwt';
import { MqttModule } from './service/mqtt/mqtt.module';
import { SystemModule } from './system/system.module';
import { BackupModule } from './backup/backup.module';

@Module({
  imports: [
    ConfigModule,
    BackupModule,
    MqttModule,
    AuthModule,
    JwtModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.getDatabaseConfig().host,
        port: configService.getDatabaseConfig().port,
        username: configService.getDatabaseConfig().username,
        password: configService.getDatabaseConfig().password,
        database: configService.getDatabaseConfig().databaseName,
        entities: [__dirname + '/**/*.entity{.ts,.js}'], // 实体类文件所在位置，自动加载实体
        synchronize: configService.getDatabaseConfig().synchronize, // 自动创建表
        logging: configService.getDatabaseConfig().logging, // 打印日志
        // 其他连接选项...
      }),
      inject: [ConfigService],
    }),
    SystemModule,
  ],
  controllers: [AppController],
  providers: [AppService, ConfigService],
})
export class AppModule {}
