import {
  Injectable,
  CanActivate,
  ExecutionContext,
  UnauthorizedException,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { InjectEntityManager } from '@nestjs/typeorm';
import { TokenExpiredError } from 'jsonwebtoken';
import { ConfigService } from 'src/config/config.service';
import { EntityManager } from 'typeorm';

@Injectable()
export class TokenAuthGuard implements CanActivate {
  private readonly config = new ConfigService();
  private readonly logger = new Logger(TokenAuthGuard.name);
  constructor(
    private readonly reflector: Reflector,
    private readonly jwtService: JwtService,
    @InjectEntityManager()
    private readonly manager: EntityManager,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    // 检查路由处理程序是否带有 @SkipTokenAuth() 装饰器
    const skipTokenAuth = this.reflector.get<boolean>(
      'skipTokenAuth',
      context.getHandler(),
    );
    if (skipTokenAuth) {
      return true; // 跳过令牌验证
    }

    // Check if the request is for Swagger UI
    if (
      request.headers['request-origion'] != null &&
      request.headers['request-origion'] == 'Knife4j'
    ) {
      const payload = {
        username: 'bfxsy',
        sub: 1,
        role: 1,
      };
      request.user = payload; // 将解码后的用户信息存储在请求中
      return true; // 跳过令牌验证
    }

    if (
      request.headers['referer'] != null &&
      request.headers['referer'].includes('swagger')
    ) {
      const payload = {
        username: 'bfxsy',
        sub: 1,
        role: 1,
      };
      request.user = payload; // 将解码后的用户信息存储在请求中
      return true; // 跳过令牌验证
    }

    // 检查请求头中是否存在授权头部信息
    const authorization = request.headers.authorization;
    if (!authorization) {
      this.logger.log(
        'TokenAuthGuard' +
          '请求IP地址为:' +
          request.connection.remoteAddress +
          ':' +
          request.connection.remotePort +
          '的用户请求了一个需要授权的接口,但是没有提供授权信息',
      );
      throw new UnauthorizedException('Missing authorization header');
    }

    // 提取令牌
    const token = authorization.replace('Bearer ', '');
    try {
      // 验证令牌
      const payload = await this.jwtService.verifyAsync(token, {
        secret: this.config.getJwtConfig().secret,
      });
      request.user = payload; // 将解码后的用户信息存储在请求中
      return true;
    } catch (err) {
      if (err instanceof TokenExpiredError) {
        // // 解码Token以获取用户信息
        // const decoded = this.jwtService.decode(token);
        // if (decoded && decoded.userId) {
        //   // 更新用户状态
        //   await this.userService.setOffline(decoded.userId);
        // }
        // const founduser = await this.manager.findOne(user, {
        //   where: {
        //     username: request.user.username,
        //   },
        // });
        // if (founduser != null && founduser != undefined) {
        //   founduser.login_state = 0;
        //   await this.manager.save(user, founduser);
        // }
        this.logger.log(
          'TokenAuthGuard' +
            '请求IP地址为:' +
            request.connection.remoteAddress +
            ':' +
            request.connection.remotePort +
            '的用户的鉴权有效时间已到,请重新登录',
        );
        throw new UnauthorizedException('Token expired');
      }
      this.logger.log(
        'TokenAuthGuard' +
          '请求IP地址为:' +
          request.connection.remoteAddress +
          ':' +
          request.connection.remotePort +
          '的用户请求了一个需要授权的接口,但是没有提供授权信息',
      );
      throw new UnauthorizedException('Invalid token');
    }
  }
}
