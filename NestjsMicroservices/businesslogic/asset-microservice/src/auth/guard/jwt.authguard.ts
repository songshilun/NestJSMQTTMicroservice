import { ExecutionContext, Injectable ,UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private readonly jwtService: JwtService) {
    super();
  }

  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest(err: any, user: any, info: any) {
    // 如果令牌过期，则返回 401 错误码 暂时交由全局异常处理 即返回200 返回数据code为-1
    if (err || !user) {
      throw err || new UnauthorizedException();
    }
    return user;
  }
}