import { Request } from 'express';

export interface User {
  username: string;
  sub: number;
  role: string;
  iat: number;
  exp: number;
}

export interface RequestWithUser extends Request {
  user: User;
}
