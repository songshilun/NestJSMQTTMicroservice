import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
const bcrypt = require('bcryptjs');

@Injectable()
export class AuthService {
  private readonly logger = new Logger('鉴权服务');
  constructor(private readonly jwtService: JwtService) {}

  async auth(username: string, userid: number, role: string) {
    const payload = {
      username: username,
      role: role,
      userid: userid,
    };
    // const options = {
    //   expiresIn: '60s', // 设置过期时间，可以使用字符串或数字
    //, options
    // };
    var token = this.jwtService.sign(payload);
    this.logger.log(`鉴权已签发,签发后的Token是:${token}`);
    return token;
  }
}
