import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { UserService } from './user.service';
import {
  CreateUserDto,
  LoginStateChange,
  ResetPassword,
  UserLoginDto,
} from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthUser, RequestWithUser } from 'src/auth/interface/requestWithUser';

@Controller()
export class UserController {
  private readonly logger = new Logger(UserController.name);
  constructor(private readonly userService: UserService) {}

  @MessagePattern('User-Create')
  create(@Payload() data) {
    const createUserDto: CreateUserDto = data.createUserDto;
    const user: AuthUser = data.user;
    return this.userService.create(createUserDto, user);
  }

  @MessagePattern('User-FindAll')
  findAll() {
    return this.userService.findAll();
  }

  @MessagePattern('User-FindOne')
  findOne(@Payload() id: number) {
    return this.userService.findOne(id);
  }

  @MessagePattern('User-Update')
  update(@Payload() data) {
    this.logger.log(data);
    const updateUserDto: UpdateUserDto = data.updateUserDto;
    const user: AuthUser = data.user;
    const id: number = data.id;
    return this.userService.update(id, updateUserDto, user);
  }

  @MessagePattern('User-Remove')
  remove(@Payload() id: number) {
    return this.userService.remove(id);
  }

  @MessagePattern('User-Login')
  login(@Payload() data) {
    const userLoginDto: UserLoginDto = data.UserLoginDto;
    const ip: string = data.ip;
    return this.userService.login(userLoginDto, ip);
  }

  @MessagePattern('User-LoginStateChange')
  loginStateChange(@Payload() LoginStateChange: LoginStateChange) {
    return this.userService.LoginStateChange(LoginStateChange);
  }

  @MessagePattern('User-ResetPassword')
  resetPassword(@Payload() ResetPassword: ResetPassword) {
    return this.userService.ResetPassword(ResetPassword);
  }
}
