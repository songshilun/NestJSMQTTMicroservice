import { Injectable, Logger } from '@nestjs/common';
import {
  CreateUserDto,
  LoginStateChange,
  ResetPassword,
  UserLoginDto,
} from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcryptjs';
import { AuthService } from 'src/auth/auth.service';
import { AuthUser, RequestWithUser } from 'src/auth/interface/requestWithUser';
import { LoginLogEntity } from './entities/login-log.entity';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';
import { ConfigService } from 'src/config/config.service';

@Injectable()
export class UserService {
  constructor(
    @InjectEntityManager()
    private manager: EntityManager,
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {}
  private client: ClientProxy;
  onModuleInit() {
    this.client = ClientProxyFactory.create({
      transport: Transport.MQTT,
      options: {
        host: this.configService.getMQTTConfig().host,
        port: this.configService.getMQTTConfig().port,
      },
    });
  }

  private readonly Logger = new Logger('登录微服务');

  async create(createUserDto: CreateUserDto, user: AuthUser) {
    try {
      this.Logger.log(user);
      createUserDto.password = createUserDto.default_password;
      createUserDto.add_user_id = user.userid;
      createUserDto.add_user = user.username;
      createUserDto.create_at = new Date();
      createUserDto.update_at = new Date();
      // 密码加盐
      createUserDto.password = bcrypt.hashSync(createUserDto.password, 10);

      const data = await this.manager.save(User, createUserDto);

      return {
        data: data,
        msg: '新增数据成功',
        code: 1,
      };
    } catch (error) {
      this.Logger.error(error);
      // 检查错误是否因唯一约束冲突引起
      if (
        error.code === 'ER_DUP_ENTRY' ||
        error.message.includes('Duplicate entry')
      ) {
        return {
          data: [],
          msg: `新增用户信息失败,请检查姓名,账号和手机号是否有重复`,
          code: -1,
        };
      }
      return {
        data: [],
        msg: `请求失败,错误信息:${error}`,
        code: -1,
      };
    }
  }

  async findAll() {
    const data = await this.manager.find(User);
    return {
      data: data,
      msg: '获取数据成功',
      code: 1,
    };
  }

  async findOne(id: number) {
    const data = await this.manager.findOne(User, {
      where: {
        id: id,
      },
    });
    return {
      data: data,
      msg: '获取单条数据成功',
      code: 1,
    };
  }

  async update(id: number, updateUserDto: UpdateUserDto, user: AuthUser) {
    try {
      this.Logger.log(updateUserDto);
      const data = await this.manager.findOne(User, {
        where: {
          id: id,
        },
      });
      Object.assign(data, updateUserDto);
      data.update_at = new Date();
      await this.manager.save(User, data);
      return {
        data: {
          result: updateUserDto,
        },
        msg: '更新数据请求完成',
        code: 1,
      };
    } catch (error) {
      this.Logger.error(error);
      // 检查错误是否因唯一约束冲突引起
      if (
        error.code === 'ER_DUP_ENTRY' ||
        error.message.includes('Duplicate entry')
      ) {
        return {
          data: [],
          msg: `更新用户信息失败,请检查姓名,账号和手机号是否有重复`,
          code: -1,
        };
      }
      return {
        data: [],
        msg: `请求失败,错误信息:${error}`,
        code: -1,
      };
    }
  }

  async remove(id: number) {
    const data = await this.manager.findOne(User, {
      where: {
        id: id,
      },
    });
    await this.manager.delete(User, data.id);
    return {
      data: {
        result: id,
      },
      msg: '删除数据请求完成',
    };
  }

  async login(userLoginDto: UserLoginDto, ip: string) {
    const username = userLoginDto.username;
    const password = userLoginDto.password;
    // 使用或条件查询用户，可能返回多个结果
    const users = await this.manager.find(User, {
      where: [
        { name: username },
        { account: username },
        { phone_number: username },
      ],
    });
    //判断user是否存在
    if (users.length == 0) {
      return {
        data: {
          user: {},
          token: '',
          status: 'fail',
        },
        code: -1,
        msg: '登录失败,并未查找到该用户',
      };
    }
    let founduser: User;
    // 遍历用户数组，尝试密码匹配
    for (const user of users) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        founduser = user;
      } else {
        this.Logger.log(`登录密码错误：${password}`);
        return {
          data: {
            user: {},
            token: '',
            status: 'fail',
          },
          code: -1,
          msg: '登录失败,密码错误',
        };
      }
    }
    if (founduser.user_state == 0) {
      //判断用户状态 如果被禁用 则不能登录
      return {
        data: {
          user: {},
          token: '',
          status: 'fail',
        },
        code: -1,
        msg: '登录失败,该用户被禁用,请联系管理员解禁',
      };
    }

    // //判断用户状态 如果被禁用 则不能登录
    // if (founduser.login_state == 1) {
    //   // 配置的鉴权有效时间（毫秒）
    //   const authTimeout = 24 * 60 * 60 * 1000; // 30分钟

    //   // 计算当前时间与登录时间的差值
    //   const currentTime = new Date().getTime();
    //   const timeDifference = currentTime - founduser.update_at.getTime();

    //   // 判断是否超过鉴权有效时间
    //   if (timeDifference <= authTimeout) {
    //     return {
    //       data: {
    //         user: {},
    //         token: '',
    //         status: 'fail',
    //       },
    //       code: -2,
    //       msg: '登录失败,已登录',
    //     };
    //   }
    // }

    this.Logger.log(`登录状态： ${founduser.login_state}`);

    var token = await this.authService.auth(
      founduser.name,
      founduser.id,
      founduser.role.toString(),
    );
    const time = new Date().getTime();

    const log = new LoginLogEntity();
    log.username = founduser.name;
    log.userId = founduser.id;
    log.loginIp = ip;
    log.loginTime = new Date();
    log.nickname = founduser.name;

    await this.manager.save(LoginLogEntity, log);

    delete founduser.password;
    // founduser.login_state = 1;
    founduser.update_at = new Date();
    await this.manager.save(User, founduser);
    return {
      data: {
        user: founduser,
        token: token,
        status: 'ok',
        token_starttime: time,
      },
      code: 1,
      msg: '登录成功',
    };
  }

  async LoginStateChange(LoginStateChange: LoginStateChange) {
    const founduser = await this.manager.findOne(User, {
      where: {
        id: LoginStateChange.userid,
      },
    });
    founduser.login_state = LoginStateChange.state ? 1 : 0;
    await this.manager.save(User, founduser);
    return {
      data: [],
      msg: '变更登录状态成功',
      code: 1,
    };
  }

  async ResetPassword(resetPassword: ResetPassword) {
    const founduser = await this.manager.findOne(User, {
      where: {
        id: resetPassword.user_id,
      },
    });
    const isMatch = await bcrypt.compare(
      resetPassword.old_password,
      founduser.password,
    );
    if (isMatch) {
      founduser.password = bcrypt.hashSync(resetPassword.new_password, 10);
      await this.manager.save(User, founduser);
      return {
        data: founduser,
        msg: '旧密码校验通过,重置密码成功',
        code: 1,
      };
    } else {
      // await this.client.emit<string>(
      //   'LogError',
      //   `[HTTP微服务]出现报错,报错的信息是:旧密码错误,请输入正确的旧密码,再重试`,
      // );
      return {
        data: founduser,
        msg: '旧密码错误,请输入正确的旧密码,再重试',
        code: -1,
      };
    }
  }
}
