import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'user',
  comment: '用户',
})
export class User {
  @PrimaryGeneratedColumn({
    comment: '用户ID',
  })
  id: number;

  @Column({
    comment: '姓名',
    type: 'tinytext',
  })
  name: string;

  @Column({
    comment: '账号',
    type: 'tinytext',
  })
  account: string;

  @Column({
    comment: '密码',
    type: 'tinytext',
  })
  password: string;

  @Column({
    comment: '角色',
  })
  role: number;

  @Column({
    comment: '关联角色表id',
  })
  ass_user_role_id: number;

  @Column({
    comment: '用户状态',
  })
  user_state: number;

  @Column({
    comment: '登陆状态',
  })
  login_state: number;

  @Column({
    comment: '手机号',
    type: 'tinytext',
  })
  phone_number: string;

  @Column({
    comment: '部门',
    type: 'tinytext',
  })
  department: string;

  @Column({
    comment: '默认密码',
    type: 'tinytext',
  })
  default_password: string;

  @Column({
    comment: '账号权限',
    type: 'tinytext',
  })
  permissions: string;

  @Column({
    comment: '操作人用户id',
  })
  add_user_id: number;

  @Column({
    comment: '操作人',
    type: 'tinytext',
  })
  add_user: string;

  @Column({
    comment: '创建时间',
  })
  create_at: Date;

  @Column({
    comment: '更新时间',
  })
  update_at: Date;
}
