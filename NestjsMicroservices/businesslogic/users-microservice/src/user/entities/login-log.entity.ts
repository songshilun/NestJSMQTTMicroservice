import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
} from 'typeorm';

@Entity({
  name: 'user_login_log',
})
export class LoginLogEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userId: number;

  @Column()
  username: string;

  @Column()
  nickname: string;

  @Column()
  loginIp: string;

  @CreateDateColumn()
  loginTime: Date;
}
