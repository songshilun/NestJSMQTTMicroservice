import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';

@Module({
  imports: [JwtModule],
  providers: [AuthService, ConfigService],
  exports: [AuthService],
})
export class AuthModule {}
