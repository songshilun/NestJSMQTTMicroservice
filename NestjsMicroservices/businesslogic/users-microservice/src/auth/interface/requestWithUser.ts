import { Request } from 'express';

export interface AuthUser {
  username: string;
  userid: number;
  role: string;
  iat: number;
  exp: number;
}

export interface RequestWithUser extends Request {
  user: AuthUser;
}
