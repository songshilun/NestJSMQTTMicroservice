import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from 'src/config/config.service';
const bcrypt = require('bcryptjs');

@Injectable()
export class AuthService {
  private readonly logger = new Logger('鉴权服务');
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async auth(username: string, userid: number, role: string) {
    const payload = {
      username: username,
      role: role,
      userid: userid,
    };
    const options = {
      expiresIn: this.configService.getJwtConfig().expiresIn, // 设置过期时间，可以使用字符串或数字
      secret: this.configService.getJwtConfig().secret,
    };
    var token = this.jwtService.sign(payload, options);
    this.logger.log(`鉴权已签发,签发后的Token是:${token}`);
    return token;
  }
}
