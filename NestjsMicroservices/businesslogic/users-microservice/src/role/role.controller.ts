import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';

@Controller()
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @MessagePattern('Role-Create')
  create(@Payload() createRoleDto: CreateRoleDto) {
    return this.roleService.create(createRoleDto);
  }

  @MessagePattern('Role-FindAll')
  findAll() {
    return this.roleService.findAll();
  }

  @MessagePattern('Role-FindOne')
  findOne(@Payload() id: number) {
    return this.roleService.findOne(id);
  }

  @MessagePattern('Role-Update')
  update(@Payload() updateRoleDto: UpdateRoleDto) {
    return this.roleService.update(updateRoleDto.id, updateRoleDto);
  }

  @MessagePattern('Role-Remove')
  remove(@Payload() id: number) {
    return this.roleService.remove(id);
  }
}
