import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'user_role',
  comment: '角色',
})
export class Role {
  @PrimaryGeneratedColumn({
    comment: '主键ID',
  })
  id: number;

  @Column({
    comment: '角色名称',
    type: 'tinytext',
  })
  role_name: string;

  @Column({
    comment: '关联权限',
    type: 'tinytext',
  })
  role_permission: string;
}
