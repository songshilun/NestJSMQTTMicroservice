import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { Role } from './entities/role.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectEntityManager()
    private manager: EntityManager,
  ) {}

  async create(createRoleDto: CreateRoleDto) {
    const data = await this.manager.save(Role, createRoleDto);
    return {
      data: data,
      msg: '新增数据成功',
      code: 1,
    };
  }

  async findAll() {
    const data = await this.manager.find(Role);
    return {
      data: data,
      msg: '获取数据成功',
      code: 1,
    };
  }

  async findOne(id: number) {
    const data = await this.manager.findOne(Role, {
      where: {
        id: id,
      },
    });
    return {
      data: data,
      msg: '获取单条数据成功',
      code: 1,
    };
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    const data = await this.manager.findOne(Role, {
      where: {
        id: id,
      },
    });
    Object.assign(data, updateRoleDto);
    await this.manager.save(data);
    return {
      data: {
        result: updateRoleDto,
      },
      msg: '更新数据请求完成',
      code: 1,
    };
  }

  async remove(id: number) {
    const data = await this.manager.findOne(Role, {
      where: {
        id: id,
      },
    });
    await this.manager.delete(Role, data.id);
    return {
      data: {
        result: id,
      },
      msg: '删除数据请求完成',
    };
  }
}
