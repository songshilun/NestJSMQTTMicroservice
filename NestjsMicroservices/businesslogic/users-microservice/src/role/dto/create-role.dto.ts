import { ApiProperty } from '@nestjs/swagger';

export class CreateRoleDto {
  @ApiProperty({
    description: '角色名称',
  })
  role_name: string;

  @ApiProperty({
    description: '关联权限',
  })
  role_permission: string;
}
