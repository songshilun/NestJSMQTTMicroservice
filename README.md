# NestJS基于MQTT的微服务

#### 介绍
基于MQTT协议的NestJS微服务架构

#### 软件架构
软件架构说明
本架构基于MQTT通讯协议实现微服务架构
具备接收HTTP请求 SocketIO长连接通讯
使用Swagger和knife4j处理接口文档

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  运行时推荐使用pm2
2.  项目内已有配置ecosystem.config 运行时 pm2 start ecosystem.config.js
3.  查看API文档 http://localhost:4500/doc.html http://localhost:4500/swagger

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
